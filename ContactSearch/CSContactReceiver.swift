//
//  ContactReceiver.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 4/19/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//

import Foundation
import UIKit


class CSContactReceiver
{
    var address:String
    var port:Int
    var name:String
    var userButton:UIButton
    var userLabel:UILabel
    var userIndex:Int
    
    internal init(addr a:String, port p:Int, button b:UIButton, label l:UILabel, name n:String, userIndex i:Int)
    {
        self.address = a
        self.port = p
        self.userButton = b
        self.userLabel = l
        self.name = n
        self.userIndex = i
    }
    func equlTo(object:AnyObject)->Bool
    {
        return object === userButton
    }
    
}