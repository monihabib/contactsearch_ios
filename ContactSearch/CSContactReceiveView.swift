//
//  CSContactReceiveView.swift
//  ContactSearch
//
//  Created by Mostafizur Rahman on 8/11/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

import UIKit

protocol DismissDelegate
{
    func willDissmissViewController()
}
class CSContactReceiveView: UIView {

    var delegate:DismissDelegate?
    @IBOutlet var contactNameLabel: UILabel!
    @IBOutlet var bottomStatusLabel: UILabel!
    @IBOutlet var topStatusLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    var activityIndicatorView:CSActivityIndicatorAnimationView =  CSActivityIndicatorAnimationView(frame: CGRectZero)
    override func drawRect(rect: CGRect) {
        
        // Drawing code
    }
 
    @IBAction func discardReceive(sender: AnyObject) {
        self.removeFromSuperview()
        delegate?.willDissmissViewController()
    }

}
