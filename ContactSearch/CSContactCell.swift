//
//  CSContactCell.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 6/19/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//

import Foundation
import UIKit
class  CSContactCell : UITableViewCell
{
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var contactCategory: UILabel!
    @IBOutlet var contactNameLabel: UILabel!
    @IBOutlet var leftiImageView: UIImageView!
    
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var nameLable: UILabel!
    
    func swipeCell(gesture:UISwipeGestureRecognizer)
    {
        print("cell swipped")
    }
}