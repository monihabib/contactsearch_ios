//
//  StartViewController.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 2/4/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//

import Foundation
import UIKit


class CSStartViewController: UIViewController//, UIViewControllerTransitioningDelegate
{
    let transition = BubbleTransition()
    var receiveContactButton: UIButton!
    var searchContactButton: UIButton!
    var sendContactButton: UIButton!
    var mainScreenSize:CGSize = UIScreen .mainScreen().bounds.size
    var widhtHeight:CGFloat = 60 * UIScreen .mainScreen().scale
    var potraitViewArray:[Int:String] = [Int:String]()
    var landscapeViewArray:[Int:String] = [Int:String]()
    var array:[Int:String] = [Int:String]()
    var transitionButton:UIButton = UIButton()
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        if(receiveContactButton == nil)
        {
            var width:CGFloat = mainScreenSize.width
            if mainScreenSize.width > mainScreenSize.height
            {
                width = mainScreenSize.height
            }
            let originXY:CGFloat =  width/2.0  - widhtHeight/2.0
            let key1 = String(format: XY_COORD,originXY, widhtHeight , widhtHeight, widhtHeight)
            let key2 = String(format: XY_COORD,originXY - widhtHeight * 0.75, widhtHeight * 2.2 , widhtHeight, widhtHeight)
            let key3 = String(format: XY_COORD,originXY + widhtHeight * 0.75, widhtHeight * 2.2 , widhtHeight, widhtHeight)
            potraitViewArray = [Int(BUTTON_TAG_SEARCH):key1,Int(BUTTON_TAG_RECEIVE):key2,Int(BUTTON_TAG_SEND):key3]
            
            if UIDevice.currentDevice().orientation.isLandscape.boolValue
            {
                let key4 = String(format: XY_COORD, widhtHeight / 2, originXY ,widhtHeight,widhtHeight)
                let key5 = String(format: XY_COORD, (mainScreenSize.width - widhtHeight) / 2, originXY,widhtHeight,widhtHeight)
                let key6 = String(format: XY_COORD,mainScreenSize.width - widhtHeight * 1.5 , originXY,widhtHeight,widhtHeight)
                landscapeViewArray = [Int(BUTTON_TAG_SEARCH):key4,Int(BUTTON_TAG_RECEIVE):key5,Int(BUTTON_TAG_SEND):key6]
                array = landscapeViewArray
            }
            else
            {
                let key4 = String(format: XY_COORD,widhtHeight/2, originXY ,widhtHeight,widhtHeight)
                let key5 = String(format: XY_COORD, (mainScreenSize.height - widhtHeight) / 2, originXY,widhtHeight,widhtHeight)
                let key6 = String(format: XY_COORD,mainScreenSize.height - widhtHeight * 1.5 , originXY,widhtHeight,widhtHeight)
                landscapeViewArray = [Int(BUTTON_TAG_SEARCH):key4,Int(BUTTON_TAG_RECEIVE):key5,Int(BUTTON_TAG_SEND):key6]
                array = potraitViewArray
            }
            self.setBoarderInLayer()
            self.updateContactButtons()
            self.updateCornerRadius()
        }
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        array = UIDevice.currentDevice().orientation.isLandscape.boolValue ? landscapeViewArray : potraitViewArray
        self.updateContactButtons()
    }
    
    func updateCornerRadius()
    {
        searchContactButton.layer.cornerRadius = widhtHeight  / 2.0
        receiveContactButton.layer.cornerRadius = widhtHeight  / 2.0
        sendContactButton.layer.cornerRadius = widhtHeight  / 2.0
    }
    
    func updateContactButtons()
    {
        for view in self.view.subviews
        {
            for (value,key) in array
            {
                if value == view.tag
                {
                    self.setViewFrame(key,view: view)
                    break
                }
            }
        }
    }
    
    func setViewFrame(key:AnyObject,view:UIView)
    {
        let valueArray:NSArray = (key as! NSString).componentsSeparatedByString("_")
        view.frame = CGRectMake(  CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(0) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(1) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(2) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(3) as! String)!))
    }
    
    internal func setBoarderInLayer()
    {
        let image = Utility.getImageFromColor(Color: UIColor.contactRed(), Size: CGSizeMake(widhtHeight, widhtHeight))
        searchContactButton = UIButton()
        sendContactButton = UIButton()
        receiveContactButton = UIButton()
        self.setButton(searchContactButton, tag: Int(BUTTON_TAG_SEARCH), image: image, title: TITLE_SEARCH)
        self.setButton(sendContactButton, tag: Int(BUTTON_TAG_SEND), image: image, title: TITLE_SEND)
        self.setButton(receiveContactButton, tag: Int(BUTTON_TAG_RECEIVE), image: image, title: TITLE_RECEIVE)

        receiveContactButton.addTarget(self, action: #selector(CSStartViewController.contactButtonActions(_:)), forControlEvents: .TouchUpInside)
        sendContactButton.addTarget(self, action: #selector(CSStartViewController.contactButtonActions(_:)), forControlEvents: .TouchUpInside)
        searchContactButton.addTarget(self, action: #selector(CSStartViewController.contactButtonActions(_:)), forControlEvents: .TouchUpInside)
        self.view.addSubview(searchContactButton)
        self.view.addSubview(receiveContactButton)
        self.view.addSubview(sendContactButton)
    }
    
    func setButton(button:UIButton, tag:Int, image:UIImage, title:String)
    {
        button.tag = tag
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        button.setTitleColor(UIColor.contactRed(), forState: UIControlState.Normal)
        button.setBackgroundImage(image, forState: .Highlighted)
        button.titleLabel!.font = UIFont(name:(searchContactButton.titleLabel?.font?.familyName)!, size: 15)
        Utility.setLayerInButton(button: button, radius: 2)
    }
    
    func contactButtonActions(sender:UIButton)
    {
        let tagValue = Int32(sender.tag)
        switch(tagValue)
        {
        case BUTTON_TAG_SEARCH:
            let masterVC:CSContactListViewController  = self.storyboard?.instantiateViewControllerWithIdentifier(VC_MASTER) as! CSContactListViewController
            masterVC.isLandScape = self.view.frame.size.width > self.view.frame.size.height
            self.navigationController?.pushViewController(masterVC, animated: true)
            break
        case BUTTON_TAG_SEND :
            let masterVC:CSContactListViewController = self.storyboard?.instantiateViewControllerWithIdentifier(VC_MASTER) as! CSContactListViewController
            masterVC.isLandScape = self.view.frame.size.width > self.view.frame.size.height
            masterVC.isSendingMode = true
            self.navigationController?.pushViewController(masterVC, animated: true)
            break
        case BUTTON_TAG_RECEIVE:
            let networkVC:CSNetworkViewController = self.storyboard?.instantiateViewControllerWithIdentifier(VC_NETWORK) as! CSNetworkViewController
            networkVC.isLandScape = self.view.frame.size.width > self.view.frame.size.height
            networkVC.isReceivingMode = true
            self.navigationController?.pushViewController(networkVC, animated: true)
            break
        default:
            break
        }
    }
}