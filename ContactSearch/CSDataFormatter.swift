//
//  DataFormatter.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 5/4/16.
//  Copyright © 2016 Dotsoft.inc. All rights reserved.
//

import Foundation


public class CSDataFormatter
{
    
    let ContactDataSize = 30
    let ContactIndexPosition = 0
    let ContactNamePosition = 4

    let DefaultDataSize = 90
    let DeviceTypePosition = 0
    let SenderIpPosition = 1
    let SenderNamePosition = 17
    let ReceiverIpPosition = 37
    let ReceiverNamePosition = 53
    let StatusPosition = 73
    let PORTPosition = 83
    
    internal var deviceType:String = ""
    internal var senderIp:String = ""
    internal var senderName:String = ""
    internal var receiverIp:String = ""
    internal var receiverName:String = ""
    internal var status:String = ""
    internal var PORT:Int = -1
    
    init(sip:String, rip:String, rname:String, sname:String, status:String, port:Int)
    {
        deviceType = "1"
        senderIp = sip
        receiverIp = rip
        senderName = sname
        receiverName = rname
        self.status = status
        PORT = port
    }
    
    init()
    {
        
    }
    
    func convertUint8ToString(DataByte buff:[UInt8], StartIndex sindex:Int, EndIndex eindex:Int)->String
    {
        let rs = buff[sindex...eindex]
        let data:[UInt8] = Array(rs)
        let nsdata =  NSData(bytes: data, length: data.count)
        return String(data: nsdata, encoding: NSUTF8StringEncoding)!
    }
    
    func convertStringToUInt(StringValue string:String, LegthObject length:Int)->[UInt8]
    {
        var str:String
        str = string.stringByPaddingToLength(length, withString: " ", startingAtIndex: 0)
        let data : NSData! = str.dataUsingEncoding(NSUTF8StringEncoding)
        let count = data.length / sizeof(UInt8)
        var array = [UInt8](count: count, repeatedValue: 0x0)
        data.getBytes(&array, length:count * sizeof(UInt8))
        return array
    }
    
    func getByteData()->[UInt8]
    {
        var sendData = [UInt8]()
        let deviceTypeData = self.convertStringToUInt(StringValue: self.deviceType, LegthObject: SenderIpPosition - DeviceTypePosition)
        sendData.appendContentsOf(deviceTypeData)
        let senderIpData = self.convertStringToUInt(StringValue: self.senderIp, LegthObject:SenderNamePosition - SenderIpPosition)
        sendData.appendContentsOf(senderIpData)
        let senderNameData = self.convertStringToUInt(StringValue: self.senderName, LegthObject:ReceiverIpPosition - SenderNamePosition)
        sendData.appendContentsOf(senderNameData)
        let receiverIpData = self.convertStringToUInt(StringValue: self.receiverIp, LegthObject:ReceiverNamePosition - ReceiverIpPosition)
        sendData.appendContentsOf(receiverIpData)
        let receiverNameData = self.convertStringToUInt(StringValue: self.receiverName, LegthObject:StatusPosition - ReceiverNamePosition)
        sendData.appendContentsOf(receiverNameData)
        let statusData = self.convertStringToUInt(StringValue: self.status, LegthObject:PORTPosition - StatusPosition)
        sendData.appendContentsOf(statusData)
        let portData = self.convertStringToUInt(StringValue: String(format: "%d", self.PORT), LegthObject:DefaultDataSize - PORTPosition)
        sendData.appendContentsOf(portData)
        //print(sendData)
        return sendData
    }
    
    func setStringFromData(ByteData data:[UInt8])
    {
        deviceType = convertUint8ToString(DataByte: data, StartIndex: DeviceTypePosition, EndIndex: SenderIpPosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        senderIp = convertUint8ToString(DataByte: data, StartIndex: SenderIpPosition, EndIndex: SenderNamePosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        senderName = convertUint8ToString(DataByte: data, StartIndex: SenderNamePosition, EndIndex: ReceiverIpPosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        receiverIp = convertUint8ToString(DataByte: data, StartIndex: ReceiverIpPosition, EndIndex: ReceiverNamePosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        receiverName = convertUint8ToString(DataByte: data, StartIndex: ReceiverNamePosition, EndIndex: StatusPosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        status = convertUint8ToString(DataByte: data, StartIndex: StatusPosition, EndIndex: PORTPosition - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        PORT = Int(convertUint8ToString(DataByte: data, StartIndex: PORTPosition, EndIndex: DefaultDataSize - 1).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))!
        //print(PORT)
    }
    //let row : [Int] = Array(tileIDs[rangeStart...rangeStart+rangeLength])
}