//
//  ReceiverListViewController.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 4/19/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork
import NetworkExtension

class CSNetworkViewController:UIViewController, TCPReceiveContactDelegate, TCPSendContactDelegate,DismissDelegate
{
    
    let RANDOM_TCP_PORT = Int(1000 + arc4random() % 8999)

    var maxWaitCoutnt = 0
    
    var sendingContactDatatLength:Int32 = 0
    
    var sendCount = -1
    
    var contactsCount = -1
    
    var isReceiveStart = false
    var isSendingStart = false
    var isReceiveEnd = false
    
    var isViewControllerDismissed = false
    var userListArray = [CSContactReceiver]()
    var receivedContactCount:Int = -1
    internal var selectedContactsArray = [CSContact]()
    var originArray = [String]()
    var hasReceiverArray = [Bool]()
    var mainScreenSize:CGSize = UIScreen .mainScreen().bounds.size
    var landscapeViewArray = [String]()
    var potraitViewArray = [String]()
    var activityIndicatorView:CSActivityIndicatorAnimationView =  CSActivityIndicatorAnimationView(frame: CGRectZero)
    var deviceIP:String = ""
    var senderIP:String = ""
    var tcpReceiveContact:TCPReceiveContact?
    var tcpSendContact:TCPContactSend?
    var receiveView :CSContactReceiveView?
    internal var isLandScape:Bool = false
    internal var isReceivingMode:Bool = false
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var wifiNameLabel: UILabel!
    var contactData:NSData = NSData()
    
    func willDissmissViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tcpSendContact = TCPContactSend()
        tcpSendContact?.sendDelegate = self
        tcpReceiveContact = TCPReceiveContact()
        tcpReceiveContact?.receiveDelegate = self
        deviceIP = Utility.getIPAddress()
        contactsCount = selectedContactsArray.count
    }
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        isViewControllerDismissed = true
        
    }
    
    func getNewReceiverInterface(name name:String, freeIndex index:Int)->(UIButton, UILabel)
    {
        var rect1:CGRect = CGRectZero
        var rect2:CGRect = CGRectZero
        let xy:NSString = originArray[index] as NSString
        let arr = (xy as NSString).componentsSeparatedByString(SEPERATOR_STR)
        rect1 = CGRectMake(CGFloat((arr[0] as NSString).doubleValue) , CGFloat((arr[1] as NSString).doubleValue)  , CGFloat(ANIM_DIMENSION) / 5, CGFloat(ANIM_DIMENSION) / 5)
        rect2 = CGRectMake(CGFloat((arr[0] as NSString).doubleValue) , CGFloat((arr[1] as NSString).doubleValue)  + 65, CGFloat(ANIM_DIMENSION) / 5, 20)
        let button = UIButton(frame: CGRectMake(bottomView.center.x - 1, bottomView.center.y - 1, 1, 1))
        button.layer.backgroundColor = UIColor.clearColor().CGColor
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGrayColor().CGColor
        button.layer.cornerRadius = 30
        button.layer.masksToBounds = true
        
        let label = UILabel(frame: CGRectMake(bottomView.center.x - 1, bottomView.center.y - 1, 1, 1))
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.view.addSubview(label)
            self.view.addSubview(button)
            button.addTarget(self, action: #selector(CSNetworkViewController.sendData(_:)), forControlEvents: .TouchUpInside)
            button.addTarget(self, action: #selector(CSNetworkViewController.actionRed(_:)), forControlEvents: .TouchDown)
            button.addTarget(self, action: #selector(CSNetworkViewController.actionBlack(_:)), forControlEvents: .TouchUpOutside)
            button.setBackgroundImage( UIImage(imageLiteral:APPLE_LOGO_RED), forState: .Normal)
            button.setBackgroundImage( UIImage(imageLiteral:APPLE_LOGO_BLACK), forState: .Highlighted)
            label.textAlignment = NSTextAlignment.Center
            label.text = name
            label.font = UIFont(name: FONT_BUTTON , size: 12)
            label.textColor = UIColor.contactRed()
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                button.frame = rect1
                label.frame = rect2
                }, completion: { (finsh) -> Void in
                    button.setBackgroundImage( UIImage(imageLiteral:APPLE_LOGO_BLACK), forState: .Normal)
                    button.setBackgroundImage( UIImage(imageLiteral:APPLE_LOGO_RED), forState: .Highlighted)
                    label.textColor = UIColor.blackColor()
            })
        }
        return (button, label)
    }
    /*animation*/
    
    func startSearchingAnimation()
    {
        
        let arr = [UIImage(imageLiteral: APPLE_LOGO_RED), UIImage(imageLiteral: ANDROID_LOGO_RED)]
        let animViewRect = CGRectMake(0, mainScreenSize.height * 0.8, mainScreenSize.width, mainScreenSize.height * 0.2)
        for i in 0...5
        {
            let square = UIImageView()
            square.image = arr[i%2]
            square.frame = CGRect(x: -40, y: 10, width: 40, height: 40)
            bottomView.addSubview(square)
            let randomYOffset = CGFloat( arc4random_uniform(UInt32(animViewRect.size.height)))
            let path = UIBezierPath()
            path.moveToPoint(CGPoint(x: -40,y:  randomYOffset ))
            path.addCurveToPoint(CGPoint(x: mainScreenSize.width > mainScreenSize.height ? mainScreenSize.width : mainScreenSize.height + 40, y:  randomYOffset ),
                                 controlPoint1: CGPoint(x: 100, y: animViewRect.size.height - randomYOffset),
                                 controlPoint2: CGPoint(x: mainScreenSize.width * 0.75 + randomYOffset, y:  randomYOffset))
            let anim = CAKeyframeAnimation(keyPath: ANIM_KEY_POSITION)
            anim.path = path.CGPath
            anim.rotationMode = kCAAnimationRotateAuto
            anim.repeatCount = Float.infinity
            anim.duration = Double(arc4random_uniform(40)+30) / 10
            anim.timeOffset = Double(arc4random_uniform(290))
            square.layer.addAnimation(anim, forKey: ANIM_KEYPATH_POSITION)
        }
    }
    
    func actionBlack(sender: UIButton) {
        let user = userListArray.filter({ $0.userButton === sender}).first
        if user != nil {
            user?.userLabel.textColor = UIColor.blackColor()
        }
    }
    
    func actionRed(sender:UIButton) {
        let user = userListArray.filter({ $0.userButton === sender}).first
        if user != nil {
            user?.userLabel.textColor = UIColor.contactRed()
        }
    }
    
    func touchDown(sender:AnyObject) {
        let user = userListArray.filter({ $0.userButton === sender}).first
        if user != nil {
            user?.userLabel.textColor = UIColor.contactRed()
        }
    }
    func touchUpOutside(sender:AnyObject)
    {
        let user = userListArray.filter({ $0.userButton === sender}).first
        if user != nil
        {
            user?.userLabel.textColor = UIColor.blackColor()
        }
    }
    
    func fetchSSIDInfo() ->  String
    {
        var currentSSID = ""
        if let interfaces:CFArray! = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafePointer<Void> = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)")
                if unsafeInterfaceData != nil {
                    let interfaceData = unsafeInterfaceData! as Dictionary!
                    currentSSID = interfaceData[SSID] as! String
                }
            }
        }
        return currentSSID
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        statusLabel.text = isReceivingMode ? STATUS_SENDR : STATUS_RECVR
        wifiNameLabel.text = STATUS_WIFI + self.fetchSSIDInfo()
        wifiNameLabel.textColor = UIColor.contactRed()
        isViewControllerDismissed = false
        self.startSearchingAnimation()
        receiveView = NSBundle.mainBundle().loadNibNamed("CSContactReceiveView", owner: self, options: nil)[0] as? CSContactReceiveView
        receiveView?.frame = UIScreen.mainScreen().bounds
        receiveView?.delegate = self
        receiveView?.topStatusLabel.numberOfLines = 0
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        let (bigValue, smallValue) = ( mainScreenSize.width > mainScreenSize.height) ? ( mainScreenSize.width, mainScreenSize.height) : (mainScreenSize.height, mainScreenSize.width)
        let width = bigValue / 6.0
        var originY1 = smallValue * 0.5 - CGFloat(ANIM_DIMENSION) / 6
        let widhtValue = 30 + width  / 2
        landscapeViewArray = isReceivingMode ? [String(format: ORG_STR,  bigValue / 2 - 150 ,smallValue / 2 - 150)] :
            [String(format: ORG_STR, width - widhtValue, originY1), String(format: ORG_STR, width * 2 - widhtValue, originY1),
             String(format: ORG_STR, width * 3 - widhtValue, originY1),  String(format: ORG_STR, width * 4 - widhtValue, originY1),
             String(format: ORG_STR, width * 5 - widhtValue, originY1), String(format: ORG_STR, width * 6 - widhtValue, originY1)]
        originY1 = bigValue * 0.4
        let originY2 = bigValue * 0.4 + 100
        potraitViewArray = isReceivingMode ? [String(format: ORG_STR,  smallValue / 2 - 150 , bigValue / 2 - 150)] :
            [String(format: ORG_STR,  20.0, originY1), String(format: ORG_STR,   smallValue / 2.0 - 30.0, originY1),
             String(format: ORG_STR,  smallValue - 80.0, originY1), String(format: ORG_STR,  20.0, originY2), String(format: ORG_STR,
                smallValue / 2.0 - 30.0, originY2), String(format: ORG_STR,  smallValue - 80.0, originY2)]
        
        originArray = UIDevice.currentDevice().orientation.isLandscape.boolValue || isLandScape ? landscapeViewArray  : potraitViewArray
        hasReceiverArray = [false, false, false, false, false, false]
        let xy = originArray[0]
        let arr = (xy as NSString).componentsSeparatedByString(SEPERATOR_STR)
        let animRect = CGRectMake(CGFloat((arr[0] as NSString).doubleValue ),
                                  CGFloat((arr[1] as NSString).doubleValue),
                                  CGFloat(ANIM_DIMENSION), CGFloat(ANIM_DIMENSION))
        if isReceivingMode
        {
            let dataFormat = CSDataFormatter(sip: deviceIP, rip: BROADCAST_ADDR, rname: "ALL",
                                           sname: UIDevice.currentDevice().name, status:STATUS_ONLINE, port: RANDOM_TCP_PORT)
            self.startUdpBroadcastclient(sendData: dataFormat)
            receiveTCPStatus()
            activityIndicatorView = CSActivityIndicatorAnimationView(frame:animRect )
            self.view.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimation()
        }
        else
        {
            self.startUdpBroadcastServer()
        }
        receiveView?.activityIndicatorView = CSActivityIndicatorAnimationView(frame:animRect )
        receiveView?.activityIndicatorView.isCircular = true
        receiveView?.addSubview((receiveView?.activityIndicatorView)!)
        receiveView?.activityIndicatorView.center = CGPointMake((receiveView?.center.x)!, (receiveView?.center.y)!)
        receiveView?.layer.borderColor = UIColor.contactRed().CGColor
        receiveView?.layer.borderWidth = 0.78
    }
   
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        originArray = UIDevice.currentDevice().orientation.isLandscape.boolValue ? landscapeViewArray : potraitViewArray
        receiveView?.activityIndicatorView.center = CGPointMake((receiveView?.center.y)!, (receiveView?.center.x)!)
        self.updateContactButtons()
    }
    
    func updateContactButtons()
    {
        if isReceivingMode
        {
            let xy = originArray[0]
            let arr = (xy as NSString).componentsSeparatedByString(SEPERATOR_STR)
            activityIndicatorView.frame = CGRectMake(CGFloat((arr[0] as NSString).doubleValue) , CGFloat((arr[1] as NSString).doubleValue)  , CGFloat(ANIM_DIMENSION), CGFloat(ANIM_DIMENSION))
            return
        }
        for view in self.view.subviews
        {
            if view is UIButton
            {
                let index = userListArray.indexOf({user in user.userButton === view})
                if index != nil
                {
                    let user = userListArray.filter({ $0.userButton === view}).first
                    let xy = originArray[index!]
                    let arr = (xy as NSString).componentsSeparatedByString(SEPERATOR_STR)
                    let rect1 = CGRectMake(CGFloat((arr[0] as NSString).doubleValue) , CGFloat((arr[1] as NSString).doubleValue), CGFloat(ANIM_DIMENSION) / 5, CGFloat(ANIM_DIMENSION) / 5)
                    let rect2 = CGRectMake(CGFloat((arr[0] as NSString).doubleValue) , CGFloat((arr[1] as NSString).doubleValue)  + 65, CGFloat(ANIM_DIMENSION)  / 5, 20)
                    user?.userButton.frame = rect1
                    user?.userLabel.frame = rect2
                }
            }
        }
    }
    
    
    func startUdpBroadcastclient(sendData dataFormat:CSDataFormatter)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), { () -> Void in
            let data = dataFormat.getByteData()
            while (!self.isViewControllerDismissed && !self.isReceiveStart){
                sleep(1)
                let udpClient:UdpSocket = UdpSocket(addr: dataFormat.receiverIp, port: Int(BROADCAST_PORT))
                udpClient.enableBroadcast()
                udpClient.send(data:data)
                udpClient.close()
            }
            if (self.isReceiveStart) {
                self.receivedContactCount = 0
                while (!self.isViewControllerDismissed && !self.isReceiveEnd)
                {
                    self.tcpReceiveContact?.receiveContact()
                }
            }
        })
    }
    
    
    func receiveTCPStatus()
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), { () -> Void in
            let server = UDPServer(addr:"", port:Int(DATA_PORT))
            while !self.isViewControllerDismissed && !self.isReceiveStart
            {
                let (data, remoteip, _) = server.recv(Int(MAX_DATA_LEN))
                if (data != nil && remoteip != self.deviceIP)
                {
                    let nsdata =  NSData(bytes: data!, length: data!.count)
                    let statusString:NSString =  NSString(data: nsdata, encoding: NSUTF8StringEncoding)!
                    let arr = statusString.componentsSeparatedByString(SEPERATOR_STR)
                    self.contactsCount = Int(arr[0]  as String)!
                    self.senderIP = arr[1] as String
                    let port = String(format: DECIMAL, self.RANDOM_TCP_PORT)
                    self.maxWaitCoutnt = 0
                    dispatch_async(dispatch_get_main_queue(), { 
                        UIApplication.sharedApplication().keyWindow?.addSubview(self.receiveView!)
                        self.receiveView?.activityIndicatorView.startAnimation()
                    })
                    while(!self.isReceiveStart && !self.isViewControllerDismissed && self.maxWaitCoutnt < 5)
                    {
                        self.isReceiveStart = (self.tcpReceiveContact?.initiateConnection(port, timeOut: 5))!
                        self.maxWaitCoutnt = self.maxWaitCoutnt + 1
                    }
                    dispatch_async(dispatch_get_main_queue(), {
                        if self.maxWaitCoutnt == 5
                        {
                            self.maxWaitCoutnt = 0
                            self.receiveView?.topStatusLabel.text = "Error in connection with " + remoteip + "."
                            self.receiveView?.bottomStatusLabel.text = "Sender : " + (arr[2] as String) + " is unreachable! Terminate receive."
                        }
                        else
                        {
                            self.receiveView?.topStatusLabel.text = "Connected with " + (arr[2] as String) + "."
                            self.receiveView?.bottomStatusLabel.text = "Reveiving " + (String(format: DECIMAL, self.contactsCount)) + " contacts."
                        }
                    })
                    
                    break
                }
            }
            server.close()
        })
    }
        
    func startUdpBroadcastServer()
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
          
            let server = UDPServer(addr:"", port:Int(BROADCAST_PORT))
            while !self.isViewControllerDismissed && !self.isSendingStart
            {
                let (data, remoteip, _) = server.recv(Int(MAX_DATA_LEN))
                if (data != nil && self.deviceIP != remoteip)
                {
                    let dataFormat = CSDataFormatter()
                    dataFormat.setStringFromData(ByteData: data!)
                    var isExistingUser = false
                    for contactReceiver in self.userListArray
                    {
                        if (contactReceiver.address == dataFormat.senderIp)
                        {
                            isExistingUser = true
                            break
                        }
                    }
                    if isExistingUser
                    {
                        continue
                    }
                    let index = self.hasReceiverArray.indexOf(false)
                    if (index != nil)
                    {
                        self.hasReceiverArray[index!] = true
                        let charCount = Int32(dataFormat.receiverName.characters.count)
                        let receiverName = charCount > MAX_LEN ? dataFormat.receiverName.substringToIndex(dataFormat.receiverName.startIndex.advancedBy(Int(MAX_LEN))) : dataFormat.receiverName
                        let (button, label) = self.getNewReceiverInterface(name : receiverName, freeIndex: index!)
                        self.userListArray.append(CSContactReceiver(addr: remoteip, port: dataFormat.PORT, button: button, label: label, name: dataFormat.receiverName, userIndex: index!))
                    }
                }
            }
            server.close()
        })
    }
    
    
    func sendData(sender:AnyObject)
    {
        //start sending loading here #ANIMATION
        UIApplication.sharedApplication().keyWindow?.addSubview(receiveView!)
        receiveView?.activityIndicatorView.startAnimation()
        isSendingStart = true
        let user = userListArray.filter({ $0.userButton === sender}).first! as CSContactReceiver
        let strignData = String(format: FORMAT, contactsCount) + deviceIP + "_" +  UIDevice.currentDevice().name
        for _ in 0...6
        {
            let udpClient:UdpSocket = UdpSocket(addr: user.address, port: Int(DATA_PORT))
            udpClient.send(str:strignData)
            udpClient.close()
        }
        Utility.delay(0.2) {
            self.sendSelectedContacts(toUser:user)
        }
    }
    
    func sendSelectedContacts(toUser user:CSContactReceiver) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            let port = String(format: DECIMAL, user.port)
            var connected = self.tcpSendContact?.initiateConnection(user.address, incommingPort: port)
            self.maxWaitCoutnt = 0
            while(!connected! && self.maxWaitCoutnt < 5)
            {
                connected = self.tcpSendContact?.initiateConnection(user.address, incommingPort: port)
                self.maxWaitCoutnt = self.maxWaitCoutnt + 1
            }
            dispatch_async(dispatch_get_main_queue(), {
                if self.maxWaitCoutnt == 5
                {
                    self.receiveView?.topStatusLabel.text = "Error connection with " + user.name + "."
                    self.receiveView?.bottomStatusLabel.text = "Receiver unreachable! Terminate send."
                }
                else
                {
                    self.receiveView?.topStatusLabel.text = "Connect receiver : " + user.name + "."
                    self.receiveView?.bottomStatusLabel.text = "Sending 1 of " + String(format: "%d",self.contactsCount) + " contacts..."
                }
                self.maxWaitCoutnt = 0
            })
            self.sendCount = 0;
            self.maxWaitCoutnt = 0
            while(!self.isViewControllerDismissed && self.contactsCount > self.sendCount && !self.isReceiveEnd )
            {
                let contactObject = self.selectedContactsArray[self.sendCount]
                dispatch_async(dispatch_get_main_queue(), {
                    self.receiveView?.contactNameLabel.text = "Sending " + (contactObject.nameFull != nil ? contactObject.nameFull! : "")
                    self.receiveView?.profileImageView.image = contactObject.imageData != nil ? UIImage(data: contactObject.imageData!) : nil
                self.receiveView?.bottomStatusLabel.text = String(format: "Sending %d of %d contacts", Int(self.sendCount + 1), self.contactsCount)
                })
                let contactData = CSJsonStructures.getDataFromContact(ContactObj:contactObject)
                self.sendingContactDatatLength = Int32((contactData?.length)!)
                self.tcpSendContact?.sendContact(contactData)
            }
        })
    }

    func onContactSendSuccess(length: Int32) {
        dispatch_async(dispatch_get_main_queue(), {
         self.receiveView?.bottomStatusLabel.text = "Sending " +  String(format: DECIMAL, self.sendCount) + " of " + String(format: DECIMAL,self.contactsCount) + " contacts..."
        })
        if length == sendingContactDatatLength {
            var statusLen = tcpSendContact?.receiveStatus()
            maxWaitCoutnt = 0
            while (statusLen == -1 && !isViewControllerDismissed) {
                statusLen = tcpSendContact?.receiveStatus()
                maxWaitCoutnt = maxWaitCoutnt + 1
            }
            if maxWaitCoutnt == 20
            {
                maxWaitCoutnt = 0
                dispatch_async(dispatch_get_main_queue(), {
                    self.receiveView?.topStatusLabel.text = "Connection fail."
                    self.receiveView?.bottomStatusLabel.text = "Receiver unreachable! Terminating..."
                })
            }
        }
    }
    
    
    
    
    func onContactReceivedSuccess(nsdata: NSData!){
        self.maxWaitCoutnt = 0
        self.receiveContacts(contactData:nsdata)
    }
    
    func onSendStatusReceived(count: Int32) {
        sendCount = Int(count)
        isReceiveEnd = sendCount == contactsCount
        print(isReceiveEnd)
    }
    
    func receiveContacts(contactData nsdata:NSData) {
        let contact:CSContact = CSJsonStructures.createContact(nsdata)
        receivedContactCount = receivedContactCount + 1
        dispatch_async(dispatch_get_main_queue(), {
            self.receiveView?.contactNameLabel.text = contact.nameFull! + " received!"
            self.receiveView?.profileImageView.image = UIImage(data: contact.imageData!)
            self.receiveView?.bottomStatusLabel.text = String(format: "Receiving %d of %d contacts", self.receivedContactCount, self.contactsCount)
        })
        Utility.saveContactToAddressBook(receiveContact: contact)
        let receiveCountString: String = String(format: DECIMAL, receivedContactCount)
        tcpReceiveContact?.sendStatus(receiveCountString)
        print(receiveCountString)
        if contactsCount == receivedContactCount {
            for _ in 0...4 {
                tcpReceiveContact?.sendStatus(receiveCountString)
            }
            isReceiveEnd = true
        }
    }
    
    func onContactReceiveError(receiveError: NSError!) {    
        let receiveCountString: String = String(format: DECIMAL, receivedContactCount)
        tcpReceiveContact?.sendStatus(receiveCountString)
        self.maxWaitCoutnt = self.maxWaitCoutnt + 1
        if self.maxWaitCoutnt == 5
        {
            self.maxWaitCoutnt = 0
            self.isReceiveEnd = true
            dispatch_async(dispatch_get_main_queue(), {
            self.receiveView?.contactNameLabel.text = ""
            self.receiveView?.topStatusLabel.text = "Connection fail."
            self.receiveView?.bottomStatusLabel.text = "Sender is closed and communication channel broken."
            })
        }
    }

    func onContactSendError(sendError: NSError!) {
        self.maxWaitCoutnt = self.maxWaitCoutnt + 1
        if self.maxWaitCoutnt == 5
        {
            self.maxWaitCoutnt = 0
            isReceiveEnd = true
            dispatch_async(dispatch_get_main_queue(), {
                self.receiveView?.topStatusLabel.text = "Connection fail."
                self.receiveView?.bottomStatusLabel.text = "Receiver is closed and communication channel broken."
            })
        }
    }
    
    func initateContactLoading(contactData nsdata:NSData)
    {
        let (deviceName, deviceIPAddress, firstContactName, totalContactCount) = CSJsonStructures.getSenderInfo(nsdata)
        contactsCount = totalContactCount
        
        receiveView?.topStatusLabel.text = String(format: "From %@\nIP:%@", deviceName, deviceIPAddress) as NSString as String
        receiveView?.bottomStatusLabel.text = String(format: "Receiving %d of %d", receivedContactCount, contactsCount)
        receiveView?.contactNameLabel.text = String(format: "%@", firstContactName)
        UIApplication.sharedApplication().keyWindow?.addSubview(self.receiveView!)
        
    }
}
