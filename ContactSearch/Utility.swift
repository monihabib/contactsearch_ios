//
//  Utility.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 3/16/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//
import Contacts
import Foundation
import UIKit
import CoreSpotlight
import MobileCoreServices

@_silgen_name("getIPAddress") func swift_getIPAddress(address: UnsafePointer<Int8>)
class Utility:CSIndexExtensionRequestHandler
{
    static func setAnimationView(withTitle:String)->(CAShapeLayer,UIView,UILabel)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let onView:UIView = UIVisualEffectView(effect: blurEffect)
        onView.frame = UIScreen.mainScreen().bounds
        onView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        onView.layer.backgroundColor = UIColor(red: 220.0/255.0, green: 40.0/255.0, blue: 90.0/255.0, alpha: 0.0).CGColor
        let layer:CAShapeLayer = CAShapeLayer()
        layer.borderWidth = 1.5;
        layer.cornerRadius = 50;
        layer.borderColor = UIColor.contactRed().CGColor
        layer.backgroundColor = UIColor.clearColor().CGColor
        layer.masksToBounds = true;
        let thePath:CGMutablePathRef = CGPathCreateMutable();
        CGPathMoveToPoint(thePath,nil,0.0,0.0);
        CGPathAddLineToPoint(thePath, nil, 50, 50);
        let rotate:CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotate.removedOnCompletion = false;
        rotate.fillMode = kCAFillModeForwards;
        rotate.toValue = (NSNumber( double: M_2_PI));
        rotate.repeatCount = Float.infinity
        rotate.autoreverses = false;
        rotate.duration = 0.5 ;
        rotate.beginTime = 0;
        rotate.cumulative = true;
        rotate.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionLinear)
        layer.frame = CGRectMake(0, 0, 100, 100)
        layer.addAnimation(rotate, forKey: "position")
        layer.strokeColor = UIColor.contactRed().CGColor
        layer.path = thePath as CGPath
        layer.position = onView.center
        onView.layer.addSublayer(layer)
        onView.tag = 1000
        let label = UILabel()
        label.text = withTitle
        label.textAlignment = .Center
        label.font = UIFont(name: (label.font?.familyName)!, size: 10)
        label.frame = CGRectMake(0, 43, 50, 14)
        onView.addSubview(label)
        label.center = layer.position
        label.textColor = UIColor.contactRed()
        UIApplication.sharedApplication().keyWindow!.addSubview(onView)
        return (layer,onView,label)
    }
    
    static func setLayerInButton(button button:UIButton, radius r:CGFloat)
    {
        button.layer.borderWidth = r
        button.layer.borderColor = UIColor.contactRed().CGColor
        button.layer.masksToBounds = true;
//        button.layer.backgroundColor = UIColor.whiteColor().CGColor
//        button.layer.cornerRadius = r * 10;
    }
    
    static func delay(delay:Double, closure:()->())
    {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    static func startSearchingAnimation(MainScreenSize mainScreenSize:CGSize, ViewRect rect:CGRect) -> UIView
    {
        let bottomView = UIView(frame: rect)
        let arr = [UIImage(imageLiteral: "applelogored"), UIImage(imageLiteral: "androidlogored")]
        let animViewRect = CGRectMake(0, mainScreenSize.height * 0.8, mainScreenSize.width, mainScreenSize.height * 0.2)
        for i in 0...5 {
            let square = UIImageView()
            square.image = arr[i%2]
            square.frame = CGRect(x: -40, y: 10, width: 40, height: 40)
            bottomView.addSubview(square)
            let randomYOffset = CGFloat( arc4random_uniform(UInt32(animViewRect.size.height)))
            let path = UIBezierPath()
            path.moveToPoint(CGPoint(x: -40,y:  randomYOffset ))
            path.addCurveToPoint(CGPoint(x: mainScreenSize.width + 40, y:  randomYOffset ),
                controlPoint1: CGPoint(x: 100, y: animViewRect.size.height - randomYOffset),
                controlPoint2: CGPoint(x: mainScreenSize.width * 0.75 + randomYOffset, y:  randomYOffset))
            let anim = CAKeyframeAnimation(keyPath: "position")
            anim.path = path.CGPath
            anim.rotationMode = kCAAnimationRotateAuto
            anim.repeatCount = Float.infinity
            anim.duration = Double(arc4random_uniform(40)+30) / 10
            anim.timeOffset = Double(arc4random_uniform(290))
            square.layer.addAnimation(anim, forKey: "animate position along path")
        }
        return bottomView
    }
    static func getIPAddress()->String
    {
        let address:[Int8] = [Int8](count:16, repeatedValue:0x0)
        swift_getIPAddress(address)
        if let ip = String(CString: address, encoding: NSUTF8StringEncoding)
        {
            return ip
        }
        return ""
    }
    
    static func getImageFromColor(Color color:UIColor, Size size:CGSize) ->UIImage
    {
        let rect  = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
    static  func getSquareImage(image: UIImage) -> UIImage
    {
        let wh = image.size.width > image.size.height ? image.size.height :  image.size.width
        let rect = CGRectMake(0, 0, wh, wh)
        let drawPoint = CGPointMake(wh - image.size.width    , wh - image.size.height )
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 1.0)
        image.drawAtPoint(drawPoint)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    
    static func getLabelValues(fromDictionary dictionary:NSMutableDictionary)->[CNLabeledValue]
    {
        let allkeys:[String] = dictionary.allKeys as! [String]
        var labelValues = [CNLabeledValue]()
        if allkeys.count > 0
        {
            for key in allkeys
            {

                let email = CNLabeledValue(label:CNLabelHome , value: CNPhoneNumber(stringValue: dictionary.valueForKey(key) as! String))
                labelValues.append(email)
            }
        }
        return labelValues
    }
    
    
    static func save()
    {
        let cncontact = CNMutableContact()
        cncontact.givenName = "Lutha"
        cncontact.familyName = "Martin"
        cncontact.phoneNumbers = [CNLabeledValue(label:CNLabelHome , value: CNPhoneNumber(stringValue: "01716777712"))]
        let store = CNContactStore()
        let saveRequest = CNSaveRequest()
        saveRequest.addContact(cncontact, toContainerWithIdentifier:nil)
        //CNLabelWork
        //CNLabelOther
        //
        do
        {
            try store.executeSaveRequest(saveRequest)
        }
        catch
        {
            print("i dunno")
        }
    }
    
    static func saveContactToAddressBook(receiveContact contact:CSContact)
    {
        let cncontact = CNMutableContact()
        //save image
        if contact.imageData != nil  && contact.imageData?.length > 0{
            cncontact.imageData = contact.imageData!
        }
         // The profile picture as a NSData object
        var labelNameArray = [CNLabelPhoneNumberiPhone, CNLabelHome, CNLabelWork, CNLabelOther, CNLabelPhoneNumberMobile, CNLabelPhoneNumberMain, CNLabelPhoneNumberHomeFax, CNLabelPhoneNumberWorkFax, CNLabelPhoneNumberOtherFax, CNLabelPhoneNumberPager]
        
        var allKeys = contact.phonenumberDictionary.allKeys as! [String]
        var lowercaselabelNameArray = labelNameArray.flatMap{$0.lowercaseString}
        var labelValue:[CNLabeledValue] = []
        for key in allKeys
        {
            let loserLabel = lowercaselabelNameArray.filter{$0.containsString(key.lowercaseString)}.first
            let label = labelNameArray[lowercaselabelNameArray.indexOf(loserLabel!)!]
            let labeledValue = CNLabeledValue(label:label , value: CNPhoneNumber(stringValue:contact.phonenumberDictionary.objectForKey(key) as! String))
            labelValue.append(labeledValue)
        }
        cncontact.phoneNumbers = labelValue
        allKeys.removeAll()
        lowercaselabelNameArray.removeAll()
        labelNameArray.removeAll()
        labelValue.removeAll()
        labelNameArray = [CNLabelWork, CNLabelOther, CNLabelHome]
        allKeys = contact.emailDictionary.allKeys  as! [String]
        for key in allKeys
        {
            let value = contact.emailDictionary.objectForKey(key) as! String
            if (value.containsString(",\n"))
            {
                let emailArray = value.componentsSeparatedByString(",\n")
                for email in emailArray
                {
                    let labeledValue = CNLabeledValue(label:CNLabelOther , value: email)
                    labelValue.append(labeledValue)
                }
                continue
            }
            
            let index = allKeys.indexOf({$0 == key})
            let labeledValue = CNLabeledValue(label:labelNameArray[index! % labelNameArray.count] , value: value)
            labelValue.append(labeledValue)
        }
        cncontact.emailAddresses = labelValue
        
        
        labelValue.removeAll()
        allKeys.removeAll()
        
        
        allKeys = contact.address.allKeys as! [String]
        for key in allKeys
        {
            
            let addressDictionary = contact.address.objectForKey(key) as! NSMutableDictionary
            let valueArray  = ["street","city","state","postalCode","country"]
            let address:CNPostalAddress = CNPostalAddress()
            for postalKey in valueArray
            {
                address.setValue(addressDictionary.objectForKey(postalKey), forKey: postalKey)
            }
            let index = allKeys.indexOf({$0 == key})
            let labeledValue = CNLabeledValue(label:labelNameArray[index! % labelNameArray.count] , value: address)
            labelValue.append(labeledValue)
        }
        cncontact.postalAddresses = labelValue
        
        
        labelValue.removeAll()
        allKeys.removeAll()
        allKeys = contact.webaddressDictionary.allKeys as! [String]
        labelNameArray.removeAll()
        labelNameArray = ["twitter", "facebook", "flickr", "linkedin"]
        var urlAddresses = [CNLabeledValue]()
        for key in allKeys
        {
            let value = (contact.webaddressDictionary.objectForKey(key) as! NSMutableDictionary).map { ($0.1 as! String) } as [String]
            if key.containsString("Social ")
            {
                let socialProfilLabel = labelNameArray.filter{$0 == key.stringByRemovingOnce("Social ")}.first
                
                let profile = CNSocialProfile.init(urlString: value[4] , username: value[2], userIdentifier: value[1], service:value[0])
                let labeledValue = CNLabeledValue(label: socialProfilLabel, value: profile)
                labelValue.append(labeledValue)
            }
            else
            {
                let labeledValue = CNLabeledValue(label: CNLabelURLAddressHomePage, value: value[0])
                urlAddresses.append(labeledValue)
                
            }
            
        }
        cncontact.urlAddresses = urlAddresses
        cncontact.socialProfiles = labelValue
//        cncontact.urlAddresses
        
        
        //save names
        cncontact.givenName = String(format:"%@", contact.nameGiven!)
        cncontact.familyName = String(format:"%@", contact.nameFamily!)
        cncontact.nickname = String(format:"%@", contact.nickname) + ""
        
        cncontact.note = contact.notes
        if contact.birthday.day > 0 {

            

            let birthday = NSDateComponents()
            birthday.day = Int(contact.birthday.day)
            birthday.month = Int(contact.birthday.month)
            birthday.year = Int(contact.birthday.year)  // You can omit the year value for a yearless birthday
            cncontact.birthday = birthday
        }
        
        
        
        let title = contact.profession["Job Title"] as! String
        let organization =  contact.profession["Organization Name"] as! String
        cncontact.jobTitle = title.containsString("not found") ? "" : title
        cncontact.organizationName = organization.containsString("not found") ? "" : organization
        
//        if (CSSearchableIndex.isIndexingAvailable())
//        {
//        
//        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeData as String)
//        // Add metadata that supplies details about the item.
//        attributeSet.title = "July Report.Numbers"
//        attributeSet.contentDescription = "iWork Numbers Document"
//        let path = NSBundle.mainBundle().pathForResource("applelogored", ofType: "png")
//        attributeSet.thumbnailData = NSData.init(contentsOfFile: path!)
//        
//        // Create an item with a unique identifier, a domain identifier, and the attribute set you created earlier.
//        let item = CSSearchableItem(uniqueIdentifier: "1", domainIdentifier: "file-1", attributeSet: attributeSet)
//        
//        // Add the item to the on-device index.
//        CSSearchableIndex.defaultSearchableIndex().indexSearchableItems([item]) { error in
//            if error != nil {
//                print(error?.localizedDescription)
//            }
//            else {
//                print("Item indexed.")
//            }
//        }
//        }
//        else
//        {
//            print("unavailable")
//        }
        
        let store = CNContactStore()
        let saveRequest = CNSaveRequest()
        saveRequest.addContact(cncontact, toContainerWithIdentifier:nil)
        
        do
        {
            try store.executeSaveRequest(saveRequest)
        }
        catch
        {
            print("i dunno")
        }
    }
    
    override func searchableIndexDidThrottle(searchableIndex: CSSearchableIndex) {
         NSLog("searchableIndexDidThrottle")
    }
    override func searchableIndexDidFinishThrottle(searchableIndex: CSSearchableIndex) {
         NSLog("searchableIndexDidFinishThrottle")
    }
    
    override func searchableIndex(searchableIndex: CSSearchableIndex, reindexAllSearchableItemsWithAcknowledgementHandler acknowledgementHandler: () -> Void) {
         NSLog("reindexAllSearchableItemsWithAcknowledgementHandler")
    }
    
    override func searchableIndex(searchableIndex: CSSearchableIndex, reindexSearchableItemsWithIdentifiers identifiers: [String], acknowledgementHandler: () -> Void) {
        NSLog("reindexSearchableItemsWithIdentifiers")
    }

    
    
    static func processContact(contact:CNContact) ->CSContact
    {
       
        let identifier = contact.identifier
        var category : String = ""
        var imageData:NSData = NSData()
        var birthday:NSDateComponents = NSDateComponents()
        let emailDictionary:NSMutableDictionary = NSMutableDictionary()
        let phonenumberDictionary:NSMutableDictionary = NSMutableDictionary()
        let achivenment:NSMutableDictionary = NSMutableDictionary()
        let familyArray = ["mo", "dad", "bro ", " bro", "sis "," sis", "sister", "brother", "uncle","younger","elder","in-law","aunty","in law"]
        let friendArray = ["frie", "fr ","frnd", "fnd ", " fr"]
        
        let webaddressDictionary:NSMutableDictionary = NSMutableDictionary()
        let notes:String = contact.note
        let addressDictionary:NSMutableDictionary = NSMutableDictionary()
        let nameGiven:NSString = contact.givenName + ""
        let nameFamily:NSString = contact.familyName + ""
        let nameNickname:NSString = contact.nickname + ""
        
        
        
        
        let nameFull:String = (nameGiven as String) + " " + (nameFamily as String) + " " + (nameNickname as String)
        let arr = nameFull.componentsSeparatedByString(" ")
        let list = arr.filter { $0 != "" }
        var displayText = ""
        if list.count > 1 {
            displayText = (list[0] as NSString).substringToIndex(1) + " " + (list[1] as NSString).substringToIndex(1)
        }else if(list.count == 1 && list[0].characters.count > 1)
        {
            displayText = String(list[0].characters.first!)
        }
        else
        {
            displayText = "N/A"
        }
        
        let lowerName = nameFull.lowercaseString + ""
     
        for family in familyArray
        {
            if lowerName.containsString(family)
            {
                category = "Family"
                break
            }
        }
        if category == ""
        {
            for friend in friendArray
            {
                if lowerName.containsString(friend)
                {
                    category = "Friends"
                }
            }
        }
        
        if (contact.imageData != nil)
        {
            imageData = contact.imageData!
        }
        
        if (contact.phoneNumbers.count != 0)
        {
            for p in contact.phoneNumbers
            {
//                CNLabeledValue(label:CNLabelPhoneNumberHomeFax , value:dictionary.valueForKey(key) as! String)
                
                let lebel:CNLabeledValue = p as CNLabeledValue
                let labelName:String = CNLabeledValue.localizedStringForLabel(lebel.label)
                let number:String = ((lebel.value as? CNPhoneNumber)?.stringValue)!
                phonenumberDictionary.setObject(number, forKey: labelName)
            }
        }
        
        if (contact.urlAddresses.count != 0)
        {
            var index:Int = 1
            for urls in contact.urlAddresses
            {
                let lebel:CNLabeledValue = urls as CNLabeledValue
                let values:String = (lebel.value as? String)!
                let nameLabel:String = CNLabeledValue.localizedStringForLabel(lebel.label)
                let valueDictionary:NSMutableDictionary = NSMutableDictionary()
                valueDictionary.setObject(values, forKey: String(format:"%@#%d",nameLabel, index))
                webaddressDictionary.setObject(valueDictionary, forKey: String(format:"%@#%d",nameLabel, index))
                index = index + 1
            }
            
        }
        
        if contact.socialProfiles.count != 0
        {
            category = "Social"
            var index:Int = 0
            for sp in contact.socialProfiles
            {
                let lebel:CNLabeledValue = sp as CNLabeledValue
                let values:CNSocialProfile = (lebel.value as? CNSocialProfile)!
                let nameLabel:String = CNLabeledValue.localizedStringForLabel(lebel.label)
                let address:NSMutableDictionary = NSMutableDictionary()
                let valueArray  = ["urlString","username","userIdentifier","service","displayname"]
                for value in valueArray
                {
                    let addressValue = values.valueForKey(value)
                    if addressValue != nil
                    {
                        address.setObject(addressValue!, forKey: value)
                    }
                    else
                    {
                        address.setObject("_", forKey: value)
                    }
                }
                if nameLabel != ""
                {
                    webaddressDictionary.setObject(address, forKey: "Social " + nameLabel)
                }
                else
                {
                    webaddressDictionary.setObject(address, forKey:String(format: "Social # %d",index))
                    index = index + 1
                }
            }
        }
        if(contact.jobTitle != "")
        {
            achivenment.setObject(contact.jobTitle, forKey: "Job Title")
        }
        else
        {
            achivenment.setObject("Job Title not found", forKey: "Job Title")
        }
        if(contact.organizationName != "")
        {
            achivenment.setObject(contact.organizationName, forKey: "Organization Name")
        }
        else
        {
            achivenment.setObject("Organization Name not found", forKey: "Organization Name")
        }
        if ((contact.birthday?.date) != nil)
        {
            birthday = contact.birthday!
        }
        if(contact.emailAddresses.count != 0)
        {
            for p in contact.emailAddresses
            {
                let lebel:CNLabeledValue = p as CNLabeledValue
                var values:String = (lebel.value as? String)!
                var keyValue:String = "email"
                if (values.containsString("@") && values.containsString("."))
                {
                    keyValue = values.sliceFrom("@", to: ".")!
                    
                }
                else if (values.containsString("www.") && values.containsString(".com"))
                {
                    keyValue = values.sliceFrom("www.", to: ".com")!
                }
                let allKeys = emailDictionary.allKeys
                for key in allKeys
                {
                    if key.containsString(keyValue)
                    {
                        category = keyValue == "facebook" || keyValue == "twitter" ? "scial" : category
                        values =   String(format: "%@,\n%@", emailDictionary.objectForKey(key) as! String, values)
                        break
                    }
                }
                emailDictionary.setObject(values, forKey: keyValue)
            }
        }
        if(contact.postalAddresses.count != 0)
        {
            let addCat = ["home#","work#","other#"]
            var count = 0
            for p in contact.postalAddresses
            {
                let lebel:CNLabeledValue = p as CNLabeledValue
                let values:CNPostalAddress = (lebel.value as? CNPostalAddress)!
                let nameLabel:String = CNLabeledValue.localizedStringForLabel(lebel.label)
                let address:NSMutableDictionary = NSMutableDictionary()
                let valueArray  = ["street","city","state","postalCode","country"]
                for value in valueArray
                {
                    let addressValue = values.valueForKey(value)
                    if addressValue != nil
                    {
                        address.setObject(addressValue!, forKey: value)
                    }
                }
                if nameLabel != ""
                {
                    addressDictionary.setObject(address, forKey: nameLabel)
                }
                else
                {
                    addressDictionary.setObject(address, forKey: addCat[count])
                    count = count + 1
                }
            }
        }
        if category == ""
        {
            category = "Other"
        }
        return CSContact(identifier: identifier, category: category ,name: nameFull ,imageData: imageData,birthday: birthday,emailDictionary: emailDictionary, phonenumberDictionary: phonenumberDictionary, achivenment: achivenment, webaddressDictionary: webaddressDictionary, notes: notes, address: addressDictionary,  nickname: nameNickname as String, givenanem: nameGiven, familyname: nameFamily, dn: displayText)
    }
    

}