import Foundation
import UIKit
import Contacts
import ContactsUI

class CSContactListViewController: UITableViewController, UISearchControllerDelegate, UIGestureRecognizerDelegate
{
    var detailViewController: CSDetailViewController? = nil
    var contacts = [CSContact]()
    var contactsArray = [CNContact]()
    var filteredContacts = [CSContact]()
    var isSearchActive:Bool = false
    var isSendActive:Bool = false
    var isAllCellSelected:Bool = false
    var previosuSearchText:String = ""
    var previousSearchScope:String = ""
    var blurEffectView:UIView = UIView()
    var rlayer:CAShapeLayer = CAShapeLayer();
    var loadingLabel:UILabel = UILabel()
    var shareViwe:UIView = UIView()
    internal var isLandScape:Bool = false
    internal var isSendingMode:Bool = false
    let mainScreenSize:CGSize = UIScreen.mainScreen().bounds.size
    var sendDataString:String?
    var searchController:UISearchController?
    var rightBarButton:UIBarButtonItem = UIBarButtonItem()
    var selectedContactsArray = [CSContact]()
    var shareViewRect:CGRect?
    var animationLayer:CAShapeLayer?
    var isLeftDirection = false
    var swippedContactName:String = ""
    var isGestureBegan = false
    var swipeLength = -1
    let util = Utility()
    let portRect = UIScreen.mainScreen().bounds.size.width >  UIScreen.mainScreen().bounds.size.height ?
        CGRectMake(0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height, 42)  :
        CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 42)
    let landRect = UIScreen.mainScreen().bounds.size.width >  UIScreen.mainScreen().bounds.size.height ?
        CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 42)  :
        CGRectMake(0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height, 42)

    
    func willDismissSearchController(searchController: UISearchController) {
        self.navigationController!.navigationBar.translucent = false
    }
    func willPresentSearchController(searchController: UISearchController) {
        self.navigationController!.navigationBar.translucent = true
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        rlayer.position  = CGPointMake(rlayer.position.y, rlayer.position.x)
        isLandScape = UIDevice.currentDevice().orientation.isLandscape.boolValue
        shareViwe.frame =  CGRectMake(0, isSendActive ? UIScreen.mainScreen().bounds.size.width - 42 : UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height, 42)
        shareViewRect = shareViwe.frame
        loadingLabel.center = rlayer.position
        let button = shareViwe.viewWithTag(2132)
        button?.center = CGPointMake(shareViwe.frame.size.width/2, (button?.center.y)!)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.definesPresentationContext = false
        tableView.alwaysBounceVertical = false
        self.tableView.multipleTouchEnabled = false
        searchController = UISearchController(searchResultsController: nil)
        searchController!.searchBar.delegate = self
        searchController!.searchResultsUpdater = self
        searchController?.searchBar.backgroundImage = UIImage()
        searchController?.searchBar.tintAdjustmentMode = UIViewTintAdjustmentMode.Normal
        
        searchController?.searchBar.barTintColor = UIColor.contactRed()
        UITableViewCell.appearance().tintColor = UIColor.contactRed()
        definesPresentationContext = true
        searchController!.dimsBackgroundDuringPresentation = false
        let textFieldInsideSearchBar = searchController!.searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.contactRed()
        searchController!.searchBar.scopeButtonTitles = ["All","Friends", "Family", "Favourite"]
        searchController!.hidesNavigationBarDuringPresentation = true
        searchController!.searchBar.tintColor = UIColor.whiteColor()
        self.tableView.tableHeaderView = searchController?.searchBar
        
        Utility.delay(0.5) { () -> () in
            self.loadContactList()
        }
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "⇐", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(CSContactListViewController.backButtonPressed(_:)))
        let font = UIFont (name: "Helvetica", size: 25)
        newBackButton.setTitleTextAttributes([NSFontAttributeName: font!], forState: .Normal)
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func backButtonPressed(sender: UIBarButtonItem)
    {
        
        let view = self.navigationController?.navigationBar.viewWithTag(1219)
        view?.removeFromSuperview()
        shareViwe.removeFromSuperview()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
    }


    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        searchController?.searchBar.backgroundImage = Utility.getImageFromColor(Color: UIColor.contactRed(), Size: CGSizeMake(600, 75))
        searchController!.active = isSearchActive
        rightBarButton = UIBarButtonItem.init(title: TITLE_SEND, style: .Plain, target: self, action: #selector(CSContactListViewController.sendContact(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.automaticallyAdjustsScrollViewInsets = true;
        if isSearchActive
        {
            searchController!.searchBar.text = previosuSearchText
            self.filterContentForSearchText(previosuSearchText, scope: previousSearchScope)
        }
        self.navigationItem.title = isSendingMode ? TITLE_CONTACT_SEND : TITLE_CONTACT
        if isSendActive || isSendingMode {
            selectedContactsArray.removeAll()
            self.navigationItem.titleView = nil
            self.tableView.reloadData()            
            isSendActive = false
        }
    }
    
    func setShareView()
    {
        shareViewRect =  self.view.bounds.size.width >  self.view.bounds.size.height ? landRect  : portRect
 
        shareViwe = UIView.init(frame: shareViewRect!)
        shareViwe.backgroundColor = UIColor.contactRed()
        let button:UIButton  = UIButton(frame: CGRectMake(mainScreenSize.width/2 - 35, 2, 70, 38))
        button.setTitle(TITLE_SEND, forState: .Normal)
        button.tag = 2132
        button.setTitleColor(UIColor.contactRed(), forState: .Highlighted)
        button.layer.borderColor = UIColor.whiteColor().CGColor;
        button.layer.borderWidth = 0.85
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        let rect = CGRectMake(0, 0, 250, 150)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(250, 150), false, 0)
        UIColor.whiteColor().setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        button.setBackgroundImage(image, forState: .Highlighted)
        button.addTarget(self, action: #selector(CSContactListViewController.shareContacts(_:)), forControlEvents: .TouchUpInside)
        shareViwe.addSubview(button)
        UIApplication.sharedApplication().keyWindow?.backgroundColor = UIColor.whiteColor()
        UIApplication.sharedApplication().keyWindow?.addSubview(shareViwe)
    }
    
    func shareContacts(sender:AnyObject)
    {
        if selectedContactsArray.count > 0 {
            shareViwe.removeFromSuperview()
            let masterVC:CSNetworkViewController = self.storyboard?.instantiateViewControllerWithIdentifier(VC_NETWORK) as! CSNetworkViewController
            masterVC.isLandScape = self.view.frame.size.width > self.view.frame.size.height
            masterVC.selectedContactsArray = selectedContactsArray
            self.navigationController?.pushViewController(masterVC, animated: true)
            shareViwe.hidden = true
        }
    }
    
    func sendContact(sender:AnyObject)
    {
        if  isSendingMode
        {
            if selectedContactsArray.count > 0 {
                let masterVC:CSNetworkViewController = self.storyboard?.instantiateViewControllerWithIdentifier(VC_NETWORK) as! CSNetworkViewController
                masterVC.isLandScape = self.view.frame.size.width > self.view.frame.size.height
                masterVC.selectedContactsArray = selectedContactsArray
                self.navigationController?.pushViewController(masterVC, animated: true)
            }
            return
        }
        isSendActive = !isSendActive
        if isSendActive
        {
            let checkmark:UIImageView = UIImageView(frame: CGRectMake(0, 0, 32, 32))
            checkmark.image = UIImage(imageLiteral: IMGNAME_CIRCLE)
            checkmark.userInteractionEnabled = true
            
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(CSContactListViewController.selectAllCells(_:)))
            gesture.numberOfTapsRequired = 1
            
            checkmark.addGestureRecognizer(gesture)
            self.navigationItem.titleView = checkmark
            rightBarButton.title = TITLE_CANCEL
            let originY = UIScreen.mainScreen().bounds.size.height - 42
            let width =  UIScreen.mainScreen().bounds.size.width
            let button = self.shareViwe.viewWithTag(2132)
            button?.center = CGPointMake(self.shareViwe.center.x, (button?.center.y)!)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.shareViwe.frame = CGRectMake(0, originY, width, 42);
                    self.view.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height - 42) ;
            })
            self.tableView.reloadData()
        }
        else
        {
            rightBarButton.title = TITLE_SEND
            self.navigationItem.titleView = nil
            self.navigationItem.title = TITLE_SEARCH
            selectedContactsArray = [CSContact]()
            let originY = UIScreen.mainScreen().bounds.size.height
            let width =  UIScreen.mainScreen().bounds.size.width
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.shareViwe.frame = CGRectMake(0, originY, width, 42) ;
                    self.view.frame = UIScreen.mainScreen().bounds
            })
            self.tableView.reloadData()
        }
    }
    
    
    
    
    
    func selectAllCells(sender:AnyObject)
    {
        let imageView:UIImageView = sender.view! as! UIImageView
        isAllCellSelected = !isAllCellSelected
        if isAllCellSelected
        {
            imageView.image = UIImage(imageLiteral: "circle-select.png")
            selectedContactsArray = isSearchActive ? filteredContacts : contacts
        }
        else
        {
            selectedContactsArray.removeAll()
            imageView.image = UIImage(imageLiteral: IMGNAME_CIRCLE)
        }        
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        self.setShareView()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
  
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
// MARK:TableView Data source delegate
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath)
    {
//        self.navigationItem.title = TITLE_CONTACT
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath?
    {
        if isSendActive || isSendingMode
        {
            var object:CSContact
            object = searchController!.active  || isSearchActive ? filteredContacts[ indexPath.row ] : contacts[indexPath.row]
            let cell:CSContactCell = tableView.cellForRowAtIndexPath(indexPath) as! CSContactCell
            let (isContains, index ) = selectedContactsArray.containsObject(object)
            
            if isContains
            {
                cell.accessoryView = .None
                selectedContactsArray.removeAtIndex(index)
            }
            else
            {
                cell.accessoryType = .Checkmark
                selectedContactsArray.append(object)
            }
            let arr = [indexPath]
            tableView.reloadRowsAtIndexPaths(arr, withRowAnimation: UITableViewRowAnimation.None)
            cell.profileImageView.backgroundColor = UIColor.randomColor()
            return indexPath
        }
        shareViwe.removeFromSuperview()
        let detailsVC:CSDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier(VC_DETAILS) as! CSDetailViewController
        detailsVC.contact = filteredContacts.count != 0 ? filteredContacts[indexPath.row] : contacts[indexPath.row]
        detailsVC.isLandScape = tableView.frame.size.width > tableView.frame.size.height
        self.navigationController?.pushViewController(detailsVC, animated: true)
        
        return indexPath;
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        let velocity:CGPoint = (gestureRecognizer as! UIPanGestureRecognizer).velocityInView(self.view)
        isLeftDirection = velocity.x < 0
        return fabs(velocity.y) < fabs(velocity.x) && !isGestureBegan
//        return gestureRecognizer.isKindOfClass(UIPanGestureRecognizer)
    }
  
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    
    func hideContactCellComponent(contactCell:CSContactCell, isHidden:Bool) {
//        contactCell.nameLable.hidden = isHidden
        contactCell.phoneLabel.hidden = isHidden
        contactCell.contactCategory.hidden = isHidden
//        contactCell.contactNameLabel.hidden = isHidden
        contactCell.leftiImageView.hidden = !isHidden
    }
    
    func rightSwipeCell(gesture:UIPanGestureRecognizer)
    {
        let contactCell = gesture.view as! CSContactCell
        if gesture.state == .Began {
            swipeLength = Int(gesture.locationInView(contactCell).x)
            isGestureBegan = true
            animationLayer = CAShapeLayer()
            animationLayer?.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width , contactCell.frame.size.height )
            animationLayer?.backgroundColor = isLeftDirection ? UIColor.brownColor().CGColor : UIColor.callingGreenColor().CGColor
            animationLayer?.name = "anim"
            contactCell.layer.insertSublayer(animationLayer!, atIndex: 0)
            hideContactCellComponent( contactCell, isHidden: true)
            contactCell.leftiImageView.image = UIImage(imageLiteral: (isLeftDirection ? "message" : "phonecall"))
            swippedContactName = swippedContactName == "" ?  contactCell.contactNameLabel.text! : swippedContactName
            contactCell.contactNameLabel.text = ( isLeftDirection ? "Mesging:" : "Calling:" ) + swippedContactName
            print("right swipe began ...")
        }
        else if(gesture.state == .Changed)
        {
            animationLayer?.backgroundColor = isLeftDirection ? UIColor.brownColor().CGColor : UIColor.callingGreenColor().CGColor
            
            if abs(swipeLength - Int(gesture.locationInView(contactCell).x)) > 50 {
                contactCell.leftiImageView.image = UIImage(imageLiteral: (isLeftDirection ? "messagered" : "phonecallred"))
            }
            else
            {
                contactCell.leftiImageView.image = UIImage(imageLiteral: (isLeftDirection ? "message" : "phonecall"))
            }
            print("right swipe changed ...")
        }
        else
        {
            animationLayer?.removeFromSuperlayer()
            swipeLength = abs(swipeLength - Int(gesture.locationInView(contactCell).x))
            let leyers = contactCell.layer.sublayers
            for  l in leyers!
            {
                if l.name == "anim" {
                    l.removeFromSuperlayer()
                }
            }
            contactCell.contactNameLabel.text = swippedContactName
            hideContactCellComponent( contactCell, isHidden: false)
            print("right swipe end...")
            swippedContactName = ""
            isGestureBegan = false
            if swipeLength > 50
            {
                let phoneNumber  = (contactCell.phoneLabel.text!.containsString("+") ? "+" : "") + contactCell.phoneLabel.text!.componentsSeparatedByCharactersInSet(
                    NSCharacterSet .decimalDigitCharacterSet() .invertedSet) .joinWithSeparator("")
                let phoneString = (isLeftDirection ? "sms:" : "telprompt://") + phoneNumber
                print(phoneString)
                let phoneURL = NSURL(string: phoneString)
                print(phoneURL)
                UIApplication.sharedApplication().openURL(phoneURL!)
            }
            
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:CSContactCell = self.tableView.dequeueReusableCellWithIdentifier(CELL_INDENTIFIER, forIndexPath: indexPath) as! CSContactCell
        let contact: CSContact
        var isContains = false
        contact = searchController!.active || isSearchActive ? filteredContacts[indexPath.row] : contacts[indexPath.row]
        (isContains,_) = selectedContactsArray.containsObject(contact)
        cell.accessoryType =  isContains ? .Checkmark : .None
        
        
        cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.size.width / 2
        cell.profileImageView.layer.masksToBounds = true
        cell.profileImageView.backgroundColor = UIColor.randomColor()
        
//        let leftSwipe = UIPanGestureRecognizer(target: self, action: #selector( CSContactListViewController.leftSwipeCell(_:)))
//        leftSwipe.direction = UIPanGestureRecognizer.Left
//        
//        cell.addGestureRecognizer( leftSwipe)

        if( cell.gestureRecognizers == nil )
        {
            let rightSwipe = UIPanGestureRecognizer(target: self, action: #selector( CSContactListViewController.rightSwipeCell(_:)))
            //        rightSwipe.direction = UIPanGestureRecognizer.Right
            rightSwipe.delegate = self
            cell.addGestureRecognizer( rightSwipe)
            
        }
        if contact.imageData?.length > 0
        {
            
//            let wh = image?.size.width > image?.size.height ? image?.size.height : image?.size.width
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
               var image = UIImage(data:contact.imageData!, scale:0.99)
            image = Utility.getSquareImage(image!)
                dispatch_async(dispatch_get_main_queue(), { 
                    cell.profileImageView.image = image
                    cell.nameLable.text = ""
                })
            })
            
        }
        else
        {
            cell.profileImageView.image = nil
            cell.profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
            cell.profileImageView.layer.borderWidth = 0.75
            cell.nameLable.text = contact.displayText
        }
        cell.contactNameLabel.text = contact.nameFull
        cell.contactCategory.text = contact.category
        let allkeys = contact.phonenumberDictionary.allKeys
        cell.phoneLabel.text = "Phone : " + (allkeys.count > 0 ? (contact.phonenumberDictionary.objectForKey(allkeys.first!) as! String): "not found")
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.contactRedSelection()
            cell.selectedBackgroundView = bgColorView
        
        
        return cell
    }
   
    
     override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchController!.active  || isSearchActive
        {
            return filteredContacts.count
        }
        let count:NSInteger = contacts.count
        return count
    }
    
    
  
    func filterContentForSearchText(searchText: String, scope: String = "All")
    {
        filteredContacts = contacts.filter(
        {
            ( contact : CSContact) -> Bool in
            if (searchText != "")
            {
                let categoryMatch = (scope == "All") || (contact.category == scope)
                return categoryMatch && contact.nameFull!.lowercaseString.containsString(searchText.lowercaseString)
            }
            else
            {
                if(scope == "All")
                {
                    return true;
                }
                let contians:Bool = contact.category.lowercaseString.containsString(scope.lowercaseString)
                return contians
            }
        })
        tableView.reloadData()
    }
    
    func findContacts() -> [CNContact]
    {
        let store = CNContactStore()
        
        let keysToFetch = [CNContactIdentifierKey, CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName),
            CNContactImageDataKey,
            CNContactBirthdayKey,
            CNContactEmailAddressesKey,
            CNContactJobTitleKey,
            CNContactNoteKey,
            CNContactDatesKey,
            CNContactUrlAddressesKey,
            CNContactSocialProfilesKey,
            CNContactPostalAddressesKey,
            CNContactOrganizationNameKey,
            CNContactFamilyNameKey,
            CNContactGivenNameKey,
            CNContactPhoneNumbersKey]
        
        let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
        
        var contacts = [CNContact]()
        
        do
        {
            try store.enumerateContactsWithFetchRequest(fetchRequest, usingBlock:
            {
                (let contact, let stop) -> Void in
                contacts.append(contact)
            })
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        return contacts
    }
    
    func loadContactList()
    {
        (rlayer,blurEffectView,loadingLabel) = Utility.setAnimationView("Loading...")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0))
        { () -> Void in
            
                self.contactsArray = self.findContacts()
                var count = 0
                for localContact in self.contactsArray
                {
                    let ct:CSContact = Utility.processContact(localContact)
                    self.contacts.append(ct)
                    count += 1
                }
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    self.tableView.reloadData()
                    self.blurEffectView.removeFromSuperview()
                    
                    //Utility.saveContactToAddressBook(receiveContact: self.contacts[9])
                }
        }
        
    }
    
    override func viewWillDisappear(animated: Bool)
    {
//        self.tableView.contentOffset = CGPointZero;
        self.automaticallyAdjustsScrollViewInsets = false;
        isSearchActive = searchController!.active
        previosuSearchText = searchController!.searchBar.text!
        searchController!.active = false
        super.viewWillAppear(animated)
    }
    
    deinit
    {
        searchController!.view.removeFromSuperview()
    }
}

extension SequenceType where Generator.Element: AnyObject {
    func containsObject(obj: Self.Generator.Element?) -> (Bool, Int) {
        if obj != nil {
            var index = 0
            for item in self {
                if item === obj {
                    return (true, index)
                }
                index += 1
            }
        }
        return (false,-1)
    }
}

extension CSContactListViewController: UISearchBarDelegate
{
  // MARK: - UISearchBar Delegate
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
    {
        
        previousSearchScope = searchBar.scopeButtonTitles![selectedScope]
        filterContentForSearchText(searchController!.searchBar.text!, scope: previousSearchScope)
    }
}

extension CSContactListViewController: UISearchResultsUpdating
{
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
//        previosuSearchText = searchBar.text!
        previousSearchScope = scope
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}




extension String {
    func sliceFrom(start: String, to: String) -> String? {
        return (rangeOfString(start)?.endIndex).flatMap { sInd in
            (rangeOfString(to, range: sInd..<endIndex)?.startIndex).map { eInd in
                substringWithRange(sInd..<eInd)
            }
        }
    }
}