/*
Contact search delegate is resposible for application startup

*/

import UIKit

@UIApplicationMain
class CSAppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool{
        return true
    }
}

extension UIColor {
    static func contactRed() -> UIColor {
        return UIColor(red: 220.0/255.0, green: 40.0/255.0, blue: 90.0/255.0, alpha: 1.0)
    }
    
    static func contactRedSelection() -> UIColor {
        return UIColor(red: 220.0/255.0, green: 40.0/255.0, blue: 90.0/255.0, alpha: 0.40)
    }
    
    static func randomColor()->UIColor {
        let hue = CGFloat(Double(arc4random() % 256) / 256.0 ) // 0.0 to 1.0
        let saturation = CGFloat(Double(arc4random() % 128) / 256.0 + 0.5) // 0.5 to 1.0, away from white
        let brightness = CGFloat(Double(arc4random() % 128) / 256.0 + 0.5) // 0.5 to 1.0, away from black
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    static func callingGreenColor() -> UIColor {
        return UIColor(red: 50.0/255.0, green: 155.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
    static func messageBrownColor() -> UIColor {
        return UIColor(red: 238.0/255.0, green: 136.0/255.0, blue: 51.0/255.0, alpha: 1.0)
    }
    func rgb() -> Int? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            return rgb
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}

extension Array {
    func contains<T where T : Equatable>(obj: T) -> Bool {
        return self.filter({$0 as? T == obj}).count > 0
    }
    
    func find (includedElement: Element -> Bool) -> Int? {
        for (idx, element) in enumerate() {
            if includedElement(element) {
                return idx
            }
        }
        return nil
    }
}

extension String {
    func stringByRemovingOnce(chars: String) -> String {
        var cs = Set(chars.characters)
        let fd = characters.filter { c in
            cs.remove(c).map { _ in false } ?? true
        }
        return String(fd)
    }
}