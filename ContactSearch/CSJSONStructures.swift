//
//  JSONStructures.swift
//  Contact Search
//
//  Created by Mostafizur Rahman on 6/27/16.
//  Copyright © 2016 Peartree Developers. All rights reserved.
//

import Foundation
import UIKit

class CSJsonStructures
{
    
    static func getDataFromContact(ContactObj contact:CSContact)->NSData?
    {
        let para:NSMutableDictionary = NSMutableDictionary()
        let values:[String:AnyObject] = ["identifier" : contact.identifier, "category" : contact.category, "names" : contact.nameFull! ,
                                         "namenickname" : contact.nickname, "namegiven" : contact.nameGiven!, "namefamily" : contact.nameFamily!,
                                         "birthday" : ["day":contact.birthday.day, "month":contact.birthday.month, "year" : contact.birthday.year],
                                         "emails" : contact.emailDictionary, "phones" : contact.phonenumberDictionary, "profession" : contact.profession,
                                         "webs" : contact.webaddressDictionary, "note" : contact.notes, "displayname" : contact.displayText, "addresses" : contact.address]
        for (key, value) in values {
            para.setValue(value, forKey: key)
        }
        let data: NSMutableData = NSMutableData()
     do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
            var dataLength: NSInteger = jsonData.length
            let lengthData = NSData(bytes:&dataLength, length:sizeof(NSInteger))
            data.appendData(lengthData)
            data.appendData(jsonData)
            if contact.imageData != nil {
                data.appendData(contact.imageData!)
            }
            return data
        } catch _ {
            return nil
        }
    }
    
    static func getSenderInfo(firstContactName name:String, totalContactCount contactCount:Int)->NSData?
    {
        let para:NSMutableDictionary = NSMutableDictionary()
        let values = ["devicename" : UIDevice.currentDevice().name,
                      "ipaddress" : Utility.getIPAddress(),
                      "firstcontactname" : name,
                      "contactcount" : contactCount]
        for (key, value) in values {
            para.setValue(value, forKey: key as! String)
        }
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
            return jsonData
        } catch _ {
            return nil
        }
    }
    
    static func getSenderInfo(jsonData:NSData)->(String, String, String, Int)
    {
        var jsonObject: [String: AnyObject]?
        do
        {
            jsonObject = try NSJSONSerialization.JSONObjectWithData(jsonData, options: []) as? [String:AnyObject]
        }
        catch let error as NSError
        {
            print(error)
        }
        let deviceName = jsonObject!["devicename"] as! String
        let deviceIPAddress = jsonObject!["ipaddress"] as! String
        let firstContactName = jsonObject!["firstcontactname"] as! String
        let totalContactCount = jsonObject!["contactcount"] as! Int
        return (deviceName, deviceIPAddress, firstContactName, totalContactCount)
    }
    
    
    static func createContact(jsonData:NSData)->CSContact
    {
        var dataLength: NSInteger = 0
        let lengthData:NSData = jsonData.subdataWithRange(NSMakeRange(0, sizeof(NSInteger)))
        lengthData.getBytes(&dataLength, length: sizeof(NSInteger))
        let data:NSData = jsonData.subdataWithRange(NSMakeRange(sizeof(NSInteger), dataLength))
        let imageData = jsonData.subdataWithRange(NSMakeRange(sizeof(NSInteger) + dataLength, jsonData.length - dataLength - sizeof(NSInteger) ))
//        if imageData.length > 0
//        {
//            let image:UIImage = UIImage(data: imageData)!
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//        }
        var jsonObject: [String: AnyObject]?
        do
        {
            jsonObject = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
        }
        catch let error as NSError
        {
            print(error)
        }
        if jsonObject != nil
        {
            
            let identifier = jsonObject!["identifier"] as! String + ""
            let category =  jsonObject!["category"] as! String + ""
            let nameNickname =  jsonObject!["namenickname"] as! String + ""
            let nameGiven =  jsonObject!["namegiven"] as! String + ""
            let nameFamily = jsonObject!["namefamily"] as! String + ""
            let name = (nameGiven as String) + " " + (nameFamily as String) + " " + nameNickname
          
            let birthday = jsonObject!["birthday"] as! [String:Int]
            let date = NSDateComponents()
            if birthday.count > 0 {
                date.month = birthday["month"]!
                date.day = birthday["day"]!
                date.year = birthday["year"]!
            }
            
            let emailDictionary = jsonObject!["emails"] as! [String:AnyObject]
            let ed = NSMutableDictionary()
            for (k, v) in emailDictionary {
                ed[k] = v
            }
            let pn = NSMutableDictionary()
            let phonenumberDictionary = jsonObject!["phones"] as! [String:AnyObject]
            for (k, v) in phonenumberDictionary {
                pn[k] = v
            }
            let profession = jsonObject!["profession"] as! NSMutableDictionary
            let web = NSMutableDictionary()
            let webaddressDictionary = jsonObject!["webs"] as! [String:AnyObject]
            for (k, v) in webaddressDictionary {
                web[k] = v
            }
            let notes = jsonObject!["note"] as! String
            let ad = NSMutableDictionary()
            let address = jsonObject!["addresses"] as! [String:AnyObject]
            for (k, v) in address {
                ad[k] = v
            }
            let displayText = jsonObject!["displayname"] as! String
            let contact = CSContact(identifier: identifier, category: category, name: name, imageData: imageData, birthday: date, emailDictionary: ed, phonenumberDictionary: pn, achivenment: profession, webaddressDictionary: web, notes: notes, address: ad, nickname: nameNickname, givenanem: nameGiven, familyname: nameFamily, dn: displayText)
            return contact
            
        }
        return CSContact.init()
    }
}