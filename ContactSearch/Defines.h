//
//  Defines.h
//  ContactSearch
//
//  Created by Mostafizur Rahman on 9/1/16.
//  Copyright © 2016 Dotsoft.inc.. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_LEN 10
#define STATUS_ONLINE "ONLINE"
#define XY_COORD "%.2f_%.2f_%.2f_%.2f"
#define TITLE_SEARCH "Search"
#define TITLE_SEND "Send"
#define TITLE_RECEIVE "Receive"
#define VC_MASTER "MasterVC"
#define VC_NETWORK "ListVC"
#define IMGNAME_CIRCLE "circle_white.png"
#define VC_DETAILS "DetailsVC"
#define CELL_INDENTIFIER "ContactCell"
#define TITLE_CONTACT "Search Contact"
#define TITLE_CONTACT_SEND "Send Contact"
#define TITLE_CANCEL "Cancel"
#define BUTTON_TAG_SEARCH  1001
#define BUTTON_TAG_SEND  1002
#define BUTTON_TAG_RECEIVE  1003


#define DATA_PORT  3535
#define ACK_PORT  45379
#define BROADCAST_PORT   3212
#define MAX_DATA_LEN  8192
#define ORG_STR  "%.0f_%.0f"
#define SEPERATOR_STR  "_"
#define APPLE_LOGO_RED  "applelogored"
#define APPLE_LOGO_BLACK  "applelogoblack"
#define ANDROID_LOGO_RED  "androidlogored"
#define ANDROID_LOGO_BLACK  "androidlogoblack"
#define FONT_BUTTON   "HelveticaNeue-UltraLight"
#define ANIM_KEY_POSITION  "position"
#define ANIM_KEYPATH_POSITION  "animate position along path"
#define SSID  "SSID"
#define BROADCAST_ADDR  "255.255.255.255"
#define STATUS_RECVR  "Searching for new receivers..."
#define STATUS_SENDR  "Waiting for sender..."
#define STATUS_WIFI  "Wifi name : "
#define ANIM_DIMENSION  300
#define FORMAT "%d_"
#define DECIMAL "%d"

@interface Defines : NSObject

@end
