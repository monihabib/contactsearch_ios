

import UIKit
import Contacts
import ContactsUI

class LCTextLayer : CATextLayer {
    
    
    
     override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(layer: aDecoder)
    }
    
    override func drawInContext(ctx: CGContext) {
        let height = self.bounds.size.height
        let fontSize = self.fontSize
        let yDiff = (height-fontSize)/2 - fontSize/10
        
        CGContextSaveGState(ctx)
        CGContextTranslateCTM(ctx, 0.0, yDiff)
        super.drawInContext(ctx)
        CGContextRestoreGState(ctx)
    }
}

class CSDetailViewController: UIViewController
{

    var VALUE_LABEL_HEIGHT:CGFloat = 20
    var DISPLAY_LABEL_HEIGHT:CGFloat = 25
    var VIEW_TAG_HORIZONTAL:Int = 1001
    var VIEW_TAG_VERTICLE:Int = 1002
    var VIEW_TAG_IMAGE:Int = 1003
    
    var SCROLLVIEW_TAG = 1212
    var BUTTON_TAG_OVERVIEW:Int = 1004
    var BUTTON_TAG_PHONE:Int = 1005
    var BUTTON_TAG_EMAIL:Int = 1006
    var BUTTON_TAG_ADDRESS:Int = 1007
    var BUTTON_TAG_WORK_EDUCATION:Int = 1008
    var BUTTON_TAG_SOCIAL:Int = 1009
    var BUTTON_TAG_DESCRIPTION:Int = 1010
    var CURRENT_BUTTON_TAG:Int = -1
    var BUTTON_TAG_WEBSITE:Int = -2
    var scrollView = UIScrollView()
    var mainScreenSize:CGSize = UIScreen .mainScreen().bounds.size
    internal var titleString:String? = String()
    internal var isLandScape:Bool = false
    internal var contact:CSContact = CSContact.init()
    var basicinfoView:UIView = UIView();
    
    var buttonArray:NSMutableArray = NSMutableArray()
    var pressedButton:UIButton = UIButton()
    var potraitViewArray:[Int:String] = [Int:String]()
    var landscapeViewArray:[Int:String] = [Int:String]()
    var array:[Int:String] = [Int:String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
  
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.title = String(format: "%@", contact.nameFull!)
        self.calculateStaticFrame()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        self.barViewAnimation()
        self.setViewForButtonTag(BUTTON_TAG_OVERVIEW)
    }

    func barViewAnimation()
    {
        for view in self.view.subviews
        {
            if view.tag == VIEW_TAG_HORIZONTAL
            {
                let rect1 = CGRectMake(view.frame.origin.x + view.frame.size.width/2, view.frame.origin.y + view.frame.size.height/2, 0, 0)
                self.setStartupAnimation(rect1, rect2: view.frame, isVerticle: isLandScape, view: view)
            }
            else if(view.tag == VIEW_TAG_VERTICLE)
            {
                let rect1 = CGRectMake(view.frame.origin.x + view.frame.size.width/2, view.frame.origin.y + view.frame.size.height/2, 0, 0)
                self.setStartupAnimation(rect1, rect2: view.frame, isVerticle: true, view: view)
            }
        }
        
    }
    
    func calculateStaticFrame()
    {
        var navheight:CGFloat = (self.navigationController?.navigationBar.frame.size.height)!
        var width = mainScreenSize.width
        var height:CGFloat = mainScreenSize.height
        if mainScreenSize.width > mainScreenSize.height
        {
            width = mainScreenSize.height
            height = mainScreenSize.width
            navheight = CGFloat(navheight * 1.3)
        }
        let buttonsWidth:CGFloat = width * 0.3
        let wh:CGFloat = CGFloat(60 * UIScreen.mainScreen().scale)
        var profileImageKey = String(format: "%.2f_%.2f_%.2f_%.2f",CGFloat((width - wh)/2), CGFloat(navheight + 3), wh, wh)
        potraitViewArray.updateValue(profileImageKey, forKey:VIEW_TAG_IMAGE)
        profileImageKey = String(format: "%.2f_%.2f_%.2f_%.2f", 5.0, CGFloat((width - wh)/2), wh, wh)
        landscapeViewArray.updateValue(profileImageKey, forKey: VIEW_TAG_IMAGE)
        var viewKey = String(format: "%.2f_%.2f_%.2f_%.2f", 0.0, CGFloat(navheight + wh + 6), CGFloat(width), CGFloat(1.5))
        potraitViewArray.updateValue(viewKey, forKey:VIEW_TAG_HORIZONTAL)
        viewKey = String(format: "%.2f_%.2f_%.2f_%.2f", CGFloat(wh + 10), navheight, CGFloat(1.5), CGFloat(width - navheight))
        landscapeViewArray.updateValue(viewKey, forKey: VIEW_TAG_HORIZONTAL)
        var buttonHeight:CGFloat = CGFloat(height - (navheight + wh + 10))
        viewKey = String(format: "%.2f_%.2f_%.2f_%.2f",CGFloat(buttonsWidth + 5), CGFloat(navheight + wh + 10), CGFloat(1.5), CGFloat(buttonHeight))
        potraitViewArray.updateValue(viewKey, forKey:VIEW_TAG_VERTICLE)
        viewKey = String(format: "%.2f_%.2f_%.2f_%.2f",CGFloat(wh + buttonsWidth + 20), CGFloat(navheight), CGFloat(1.5), CGFloat(width - navheight))
        landscapeViewArray.updateValue(viewKey, forKey: VIEW_TAG_VERTICLE)
        viewKey = String(format: "%.2f_%.2f_%.2f_%.2f",CGFloat(buttonsWidth + 10), CGFloat(navheight + wh + 10), CGFloat(width - buttonsWidth - 15), CGFloat(height - navheight - wh - 15))
        potraitViewArray.updateValue(viewKey, forKey:SCROLLVIEW_TAG)
        viewKey = String(format: "%.2f_%.2f_%.2f_%.2f",CGFloat(wh + buttonsWidth + 25), CGFloat(navheight), CGFloat(height - wh - buttonsWidth - 30), CGFloat(width - navheight - 5))
        landscapeViewArray.updateValue(viewKey, forKey: SCROLLVIEW_TAG)
        let buttonTagsArray = [BUTTON_TAG_OVERVIEW,BUTTON_TAG_PHONE,BUTTON_TAG_EMAIL,BUTTON_TAG_ADDRESS,BUTTON_TAG_WORK_EDUCATION,BUTTON_TAG_SOCIAL,BUTTON_TAG_DESCRIPTION]
        let buttonCount:NSInteger =  buttonTagsArray.count
        buttonHeight = CGFloat(Int(buttonHeight) / (buttonCount + 1))
        var originY:CGFloat = navheight + wh + 10 + buttonHeight/2
        var originLandscapeY:CGFloat = 5 + navheight
        let buttonHeightLand:CGFloat = CGFloat(Int(width - navheight) / (buttonCount + 1))
        for tags in buttonTagsArray
        {
            viewKey = String(format: "%.2f_%.2f_%.2f_%.2f", 0.0, CGFloat(originY), CGFloat(buttonsWidth), CGFloat(buttonHeight))
            potraitViewArray.updateValue(viewKey, forKey:tags)
            viewKey = String(format: "%.2f_%.2f_%.2f_%.2f",wh + 16.5, originLandscapeY, buttonsWidth, buttonHeightLand)
            landscapeViewArray.updateValue(viewKey, forKey: tags)
            originY = originY + buttonHeight + 3
            originLandscapeY = originLandscapeY + buttonHeightLand + 3
        }
        if UIDevice.currentDevice().orientation.isLandscape.boolValue || isLandScape
        {
            array = landscapeViewArray
        }
        else
        {
            array = potraitViewArray
        }
        self.setupUserInterface()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        isLandScape = UIDevice.currentDevice().orientation.isLandscape.boolValue
        if isLandScape
        {
            array = landscapeViewArray
        }
        else
        {
            array = potraitViewArray
        }
        self.updateUserInterface()
        self.removeSubviews()
        self.setViewForButtonTag(CURRENT_BUTTON_TAG)
        self.barViewAnimation()
    }
    
    
    func updateUserInterface()
    {
        for view in self.view.subviews
        {
            for (value,key) in array
            {
                if value == view.tag
                {
                    self.setViewFrame(key,view: view)
                    break
                }
            }
        }
    }
    
    func setViewFrame(key:AnyObject,view:UIView)
    {
        let valueArray:NSArray = (key as! NSString).componentsSeparatedByString("_")
        view.frame = CGRectMake(  CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(0) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(1) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(2) as! String)!),
            CGFloat(NSNumberFormatter().numberFromString(valueArray.objectAtIndex(3) as! String)!))
    }
    
    func setupUserInterface()
    {
        let buttonNameArray = ["Overview", "Phone", "Emails", "Address", "Profession", "Social", "Descriptions" ]
        let rect = CGRectMake(0, 0, 150, 70)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(150, 70), false, 0)
        UIColor.contactRed().setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        for (value,key) in array
        {
            if value == VIEW_TAG_HORIZONTAL || value == VIEW_TAG_VERTICLE
            {
                self.setBarViews(value, key: key)
            }
            else if(value == VIEW_TAG_IMAGE)
            {
                self.setUserProfileImage(value,key: key)
            }
            else if(value == SCROLLVIEW_TAG)
            {
                self.setScrollView(value,key: key)
            }
            else
            {
                self.setLeftSitemapButtons(value,key: key, buttonTitle: buttonNameArray[value%1000 - 4],image:image)
            }
        }
    }
    
    func setScrollView(value:Int,key:String)
    {
        scrollView = UIScrollView()
        self.setViewFrame(key,view:self.scrollView)
        self.scrollView.tag = value
        self.scrollView.indicatorStyle = .Black
        self.scrollView.scrollEnabled = true;
        self.scrollView.showsVerticalScrollIndicator = true;
        self.view.addSubview(scrollView)
    }
    
    
    func setLeftSitemapButtons(value:Int,key:String, buttonTitle:String,image:UIImage)
    {
        let button:UIButton = UIButton()
        button.tag = value
        button.setTitle(buttonTitle, forState: .Normal)
        button.titleLabel!.font = UIFont(name:(button.titleLabel?.font?.familyName)!, size: 13)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        button.setTitleColor(UIColor.contactRed(), forState: UIControlState.Normal)
        button.setBackgroundImage(image, forState: .Highlighted)
        button.addTarget(self, action: #selector(CSDetailViewController.buttonActionUp(_:)), forControlEvents: .TouchUpInside)
        button.addTarget(self, action: #selector(CSDetailViewController.buttonActionDown(_:)), forControlEvents: .TouchDown)
        button.layer.masksToBounds = true
        self.view.addSubview(button)
        self.setViewFrame(key,view:button)
        buttonArray.addObject(button)
        if(value == BUTTON_TAG_OVERVIEW)
        {
            pressedButton = button
            pressedButton.highlighted = true
        }
    }

    func setBarViews(value:Int,key:String)
    {
        let view:UIView = UIView()
        view.tag = value
        view.layer.backgroundColor = UIColor.contactRed().CGColor
        self.view.addSubview(view)
        self.setViewFrame(key,view:view)
    }
    
    func setUserProfileImage(value:Int,key:String)
    {
        let profileImageView:UIImageView = UIImageView()
        profileImageView.layer.masksToBounds = true;
        profileImageView.contentMode = UIViewContentMode.ScaleAspectFit;
        profileImageView.tag = value
        self.setViewFrame(key,view: profileImageView)
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2;
        
        if contact.imageData != nil {
            if (contact.imageData!.length != 0)
            {
                let data:NSData = contact.imageData!
                let image:UIImage = UIImage(data: data)!
                profileImageView.image = image
            }
            
        }
        else
        {
            let color:UIColor = UIColor.randomColor()
            let myAttributes = [
                
                NSFontAttributeName: UIFont(name: "Chalkduster", size: 30.0)! , // font
                NSForegroundColorAttributeName:    color        // text color
            ]
            let myAttributedString = NSAttributedString(string: contact.displayText, attributes: myAttributes )
            
            // Text layer
            let myTextLayer = LCTextLayer.init()
            myTextLayer.alignmentMode = kCAAlignmentCenter
            myTextLayer.string = myAttributedString
            let rgb = color.rgb()
            myTextLayer.backgroundColor = UIColor(red: 1 - CGFloat((rgb! & 0xFF0000) >> 16) / 255.0,
                                                  green:  1 -   CGFloat((rgb! & 0x00FF00) >> 8) / 255.0,
                                                  blue:  1 - CGFloat(rgb! & 0x0000FF) / 255.0,
                                                  alpha: CGFloat(1.0)).CGColor
            myTextLayer.frame = profileImageView.layer.bounds
            myTextLayer.position = CGPointMake(profileImageView.frame.size.width / 2, profileImageView.frame.size.height / 2)
          
            profileImageView.layer.addSublayer(myTextLayer)
        }
        self.view.addSubview(profileImageView)
    }
    
    func setStartupAnimation(rect1:CGRect, rect2:CGRect, isVerticle:Bool, view:UIView )
    {
        view.frame = rect1
        view.layer.backgroundColor = UIColor.contactRed().CGColor
        UIView.animateWithDuration(0.6, animations: { () -> Void in
            view.frame = rect2
            let maskLayer:CAShapeLayer  = CAShapeLayer()
            let path:UIBezierPath = UIBezierPath()
            if isVerticle
            {
                path.moveToPoint(CGPointMake(0.75,0 ))
                path.addLineToPoint(CGPointMake(0, rect2.size.height * 0.5))
                path.addLineToPoint(CGPointMake(0.75, rect2.size.height))
                path.addLineToPoint(CGPointMake(1.5, rect2.size.height * 0.5))
                path.addLineToPoint(CGPointMake(0.75, 0.0))
            }
            else
            {
                path.moveToPoint(CGPointMake(0, 0.75))
                path.addLineToPoint(CGPointMake(rect2.size.width * 0.5 , 0))
                path.addLineToPoint(CGPointMake(rect2.size.width , 0.75))
                path.addLineToPoint(CGPointMake(rect2.size.width * 0.5 , 1.5))
                path.addLineToPoint(CGPointMake(0, 0.75))
            }
            maskLayer.path = path.CGPath
            view.layer.mask = maskLayer
            view.layer.masksToBounds = true
            })
    }
    
    
    
    func buttonActionDown(button:UIButton)
    {
        pressedButton.highlighted = false
        button.highlighted = true
        pressedButton = button
    }
    
    func buttonActionUp(button:UIButton)
    {
        setViewForButtonTag(button.tag)
        delay(0.005) { () -> () in
            self.pressedButton.highlighted = true
        }
    }
    
    func delay(delay:Double, closure:()->())
    {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func setViewForButtonTag(tag:Int)
    {
        var height:CGFloat = 0
        CURRENT_BUTTON_TAG = tag
            removeSubviews()
            switch(tag)
            {
            case BUTTON_TAG_OVERVIEW :
                height = setOverviewInformation()
                break
            case BUTTON_TAG_PHONE :
                height = setPhoneNumbers()
                break
            case BUTTON_TAG_EMAIL :
                height = setEmails()
                break
            case BUTTON_TAG_ADDRESS:
                height = setAddress()
                break
            case BUTTON_TAG_WORK_EDUCATION :
                height = setWorkAndEducation()
                break
            case BUTTON_TAG_SOCIAL :
                height = setSocialInfo()
                break
            case BUTTON_TAG_WEBSITE :
                break
            case BUTTON_TAG_DESCRIPTION:
                height = setDescriptions()
                break
            default:
                break
            }
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, height)
    }
    
    func setOverviewInformation()->CGFloat
    {
        var originY:CGFloat = getTitleLabel(String(format: "%@'s Overview",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        originY = self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.phonenumberDictionary, headerText: "Phone # :", notAvailableText: "Phone numbers", inputView: scrollView) + 5
        originY = self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.emailDictionary, headerText: "Emails :", notAvailableText: "Email ", inputView: scrollView) + 5
        let location:NSMutableDictionary = NSMutableDictionary()
        let allKeys = contact.address.allKeys
        if(allKeys.count != 0)
        {
            for key in allKeys {
                let dictionary = contact.address.objectForKey(key)
                let locationString = String(format: "%@, %@", (dictionary?.objectForKey("city"))! as! String, (dictionary?.objectForKey("country"))! as! String)
                location.setObject(locationString, forKey: "CITY")
                break
            }
            
        }
        return self.setLabelTemplate(10, originY: originY, width: width, dictionary: location, headerText: "Location :", notAvailableText: "Location ", inputView: scrollView) + 5
    }
    
    func setPhoneNumbers()->CGFloat
    {
        let originY:CGFloat = getTitleLabel(String(format: "%@'s Phone#",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        return self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.phonenumberDictionary, headerText: "Phone # :", notAvailableText: "Numbers",inputView: scrollView) + 5
    }
    
    func setEmails()->CGFloat
    {
        let originY:CGFloat = getTitleLabel(String(format: "%@'s Emails",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        return  self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.emailDictionary, headerText: "Emails :", notAvailableText: "Email ", inputView: scrollView) + 5
    }
    
    func setAddress()->CGFloat
    {
        var originY:CGFloat = getTitleLabel(String(format: "%@'s Address",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        let allKeys = contact.address.allKeys
        if (allKeys.count != 0)
        {
            for key in allKeys
            {
                originY = self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.address.objectForKey(key) as! NSMutableDictionary, headerText: String(format: "Location (%@):",key as! String), notAvailableText: "Location " , inputView: scrollView) + 10
            }
        }
        else
        {
            originY = self.setLabelTemplate(10, originY: originY, width: width - 20, dictionary: contact.address, headerText: "Location :", notAvailableText: "Location ",inputView: scrollView)
        }
        return originY
    }
    
    func setWorkAndEducation()->CGFloat
    {
        let originY:CGFloat = getTitleLabel(String(format: "%@'s Profession",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        return self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.profession, headerText: "Job details :", notAvailableText: "Job title ", inputView: scrollView) + 5
    }
    
    func setSocialInfo()->CGFloat
    {
        var originY:CGFloat = getTitleLabel(String(format: "%@'s Social",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = self.scrollView.frame.size.width - 20
        let allKeys = contact.webaddressDictionary.allKeys
        if (allKeys.count != 0)
        {
            for key in allKeys
            {
                originY = self.setLabelTemplate(10, originY: originY, width: width, dictionary: contact.webaddressDictionary.objectForKey(key) as! NSMutableDictionary, headerText: String(format: "Social Web (%@):",key as! String), notAvailableText: "Websites " , inputView: scrollView) + 10
            }
        }
        else
        {
            originY = self.setLabelTemplate(10, originY: originY, width: width - 20, dictionary: contact.webaddressDictionary, headerText: "Social Web :", notAvailableText: "Websites ",inputView: scrollView) + 5
        }
        return originY
    }
    
    func setDescriptions()->CGFloat
    {
        let originY:CGFloat = getTitleLabel(String(format: "%@'s Note",(contact.nameFull?.componentsSeparatedByString(" ")[0])!))
        let width:CGFloat = scrollView.frame.size.width - 20
        let label = UILabel()
        label.frame = CGRectMake(10, originY, width, scrollView.frame.size.height - originY - 20)
        label.numberOfLines = 0
        label.font = UIFont(name:(label.font?.familyName)!, size: 11)
        label.textAlignment = .Justified
        if contact.notes != ""
        {
            label.numberOfLines = 0
            label.text = contact.notes
        }
        else
        {
            label.text = String(format: "%@ is not sharing any description / note",contact.nickname)
        }
        scrollView.addSubview(label)
        self.setBottomLineView(CGRectMake(10, UIScreen.mainScreen().bounds.size.height - 12, width, 0.8), inputView: scrollView)
        return originY
    }
    
    func removeSubviews()
    {
        for view in scrollView.subviews
        {
            if (view.isKindOfClass(UIImageView))
            {
                continue
            }
            view.removeFromSuperview()
        }
    }
    
    func getTitleLabel(titletext:String)->CGFloat
    {
        let titleLabel:UILabel = UILabel()
        titleLabel.frame = CGRectMake(10, 10, scrollView.frame.size.width - 20, 30)
        titleLabel.text = titletext
        titleLabel.textColor = UIColor.contactRed()
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.font = UIFont(name:(titleLabel.font?.familyName)!, size: 17)
        scrollView.addSubview(titleLabel)
        return titleLabel.frame.origin.y + titleLabel.frame.size.height + 10
    }
    
    func getHeaderTitle(titletext:String, rectangle:CGRect)->UILabel
    {
        let titleLabel:UILabel = UILabel()
        titleLabel.frame = rectangle
        titleLabel.text = titletext
        titleLabel.textAlignment = .Left
        titleLabel.font = UIFont(name:(titleLabel.font?.familyName)!, size: 16)
        return titleLabel
    }
    
    func getLabelTitle(titletext:String, rectangle:CGRect)->UILabel
    {
        let titleLabel:UILabel = UILabel()
        titleLabel.frame = rectangle
        titleLabel.text = titletext
        titleLabel.textColor = UIColor.contactRed()
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont(name:(titleLabel.font?.familyName)!, size: 15)
        return titleLabel
    }
    
    func getValueLabel(titletext:String, rectangle:CGRect)->UILabel
    {
        let titleLabel:UILabel = UILabel()
        titleLabel.frame = rectangle
        titleLabel.text = titletext
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont(name:(titleLabel.font?.familyName)!, size: 14)
        return titleLabel
    }
    
    func setBottomLineView(rectangle:CGRect,inputView:UIView)
    {
        let view = UIView()
        view.frame = rectangle
        view.layer.backgroundColor = UIColor.contactRed().CGColor
        inputView.addSubview(view)
    }
    
    func setLabelTemplate(originX:CGFloat, originY:CGFloat, width:CGFloat, dictionary:NSMutableDictionary, headerText:String, notAvailableText:String, inputView:UIView) -> CGFloat
    {
        let phoneLabel = self.getHeaderTitle(headerText, rectangle: CGRectMake(originX, originY, width, 20))
        inputView.addSubview(phoneLabel)
        var originYCoord = originY + 20
        let allKeys = dictionary.allKeys
        if (allKeys.count != 0)
        {
            for key in allKeys
            {
                let label = self.getLabelTitle(key as! String, rectangle: CGRectMake(originX, originYCoord, width, 18))
                inputView.addSubview(label)
                originYCoord = originYCoord + 18
                let valueString:String = dictionary.objectForKey(key) as! String
                let charLength = valueString.characters.count
                if (charLength > 25)
                {
                    var dynamicHeight = CGFloat(Double(charLength) / 25.0)
                    #if arch(x86_64) || arch(arm64)
                        dynamicHeight = ceil(dynamicHeight)
                    #else
                        dynamicHeight = CGFloat(ceilf(Float(dynamicHeight)))
                    #endif
                    let valueLabel = self.getValueLabel(valueString as String, rectangle: CGRectMake(originX, originYCoord , width, dynamicHeight * 18))
                    valueLabel.numberOfLines = 0
                    inputView.addSubview(valueLabel)
                    originYCoord = originYCoord + CGFloat(dynamicHeight * 18)
                }
                else
                {
                    let valueLabel = self.getValueLabel(valueString as String, rectangle: CGRectMake(originX, originYCoord, width, 20))
                    inputView.addSubview(valueLabel)
                    originYCoord = originYCoord + 20
                }
            }
        }
        else
        {
            let label = self.getLabelTitle(String(format: "%@ not available!", notAvailableText), rectangle: CGRectMake(originX, originYCoord, width, 15))
            inputView.addSubview(label)
            originYCoord = originYCoord + 18;
        }
        self.setBottomLineView(CGRectMake(originX, originYCoord, width, 0.8),inputView: inputView)
        return originYCoord
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        for view in self.view.subviews
        {
            if view.isKindOfClass(UIImageView)
            {
                (view as! UIImageView).image = nil;
            }
            view.removeFromSuperview()
        }
    }
}
    