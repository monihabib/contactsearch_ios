/*
* Copyright (c) 2015 Dots Inc
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software restriction, including limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*/

import UIKit
import Foundation
import Contacts
class CSContact
{
    let identifier: String
    let category : String
    let nameFull:String?
    let nameGiven:NSString?
    let nameFamily:NSString?
    let nickname : String
    let imageData:NSData?
    let birthday:NSDateComponents
    let emailDictionary:NSMutableDictionary
    let phonenumberDictionary:NSMutableDictionary
    let profession:NSMutableDictionary //job title, organization name
    let webaddressDictionary:NSMutableDictionary //social profile and url address key
    let notes:String
    let address:NSMutableDictionary
    
    let displayText:String
    init (identifier:String, category : String,name : String,imageData:NSData, birthday:NSDateComponents, emailDictionary:NSMutableDictionary, phonenumberDictionary:NSMutableDictionary, achivenment:NSMutableDictionary , webaddressDictionary:NSMutableDictionary , notes:String, address:NSMutableDictionary, nickname:String, givenanem:NSString, familyname:NSString, dn:String)
    {
        self.identifier = identifier
        self.category   = category
        self.nameFull  = name
        self.nameGiven = givenanem
        self.nameFamily = familyname
        self.imageData = imageData.length > 0 ? imageData : nil
        self.birthday = birthday
        self.emailDictionary = emailDictionary
        self.phonenumberDictionary = phonenumberDictionary
        self.profession = achivenment //job title, organization name
        self.webaddressDictionary = webaddressDictionary //social profile and url address key
        self.notes = notes
        self.address = address
        self.nickname = nickname
        self.displayText = dn
    }
    
    init()
    {
        self.category   = ""
        self.nameFull  = ""
        self.nameFamily = ""
        self.nameGiven = ""
        self.imageData = nil
        self.birthday = NSDateComponents()
        self.emailDictionary = NSMutableDictionary()
        self.phonenumberDictionary = NSMutableDictionary()
        self.profession = NSMutableDictionary() //job title, organization name
        
        self.webaddressDictionary = NSMutableDictionary() //social profile and url address key
        self.notes = ""
        self.address = NSMutableDictionary()
        self.nickname = ""
        self.identifier = ""
        self.displayText = ""
    }
}
